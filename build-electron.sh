# Exit immediately upon error
set -e

echo 'Removing existing output-folder'
rm -rf electron-build

echo 'Building Angular...'
npm run build-for-electron

echo 'Packaging Electron...'
npm run build-electron

echo 'Generate zip file...'
cd electron-build/
jar -cfM zeit-tool-win32-x64.zip zeit-tool-win32-x64