# Zeit-Tool

[![Netlify Status](https://api.netlify.com/api/v1/badges/60d260c3-9697-4f5d-8322-89c3b3ec427e/deploy-status)](https://app.netlify.com/sites/zeit-tool/deploys)

Webapp zum Berechnen und Anzeigen der täglichen Arbeitszeit.

## ONLINE VERSION

**release** https://zeit-tool.netlify.com/  
**develop** https://zeit-tool-dev.netlify.com/

## REQUIREMENTS

> [NodeJS](https://nodejs.org/en/)  
> [Poedit](https://poedit.net/) (optional, for i18n)

> `$ git clone https://gitlab.com/ric0/zeit-tool.git`  
> `$ npm install`  
> `$ npm run postinstall`

##### OPTIONAL:

For Angular CLI usage or _Code scaffolding_ (see below)

> `$ npm install -g @angular/cli`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically 
reload if you change any of the source files.  
To run a localized version use one of the following commands:
> `$ ng s --configuration=en`  --> `http://localhost:4200/en`  
> `$ ng s --configuration=de`  --> `http://localhost:4200/de`
  

Local http-server (requires compiled output `./build.sh`):

> `$ npx http-server@0.9.0 -p 4200 -c-1 dist/en`

## i18n
This project primarily uses the vanilla `Angular i18n` implementation, to inject static strings. But the Angular i18n has some 
limitations, eg. passing strings to components is impossible, thus `ngx-translate` is used to cover some of 
Angular's shortcomings. 

Currently supported languages: `DE`, `EN`  

Required steps to add strings for translations:  
**[Angular I18N]**
- add the `i18n` directive to an element, eg. `<span i18n="@@key">Hello World</span>`
- `npm run xi18n` or `ng xi18n` - Extracts the strings for translation and merges the changes 
   automatically into the existing locale-files.
- Translate the strings. Program used to translate the files: https://poedit.net/
- `./build.sh` Rebuild

**[ngx-translate]**
- Add key-value pairs to the src/assets/i18n/*.json files. (value == translation)
- usage: https://github.com/ngx-translate/core#4-use-the-service-the-pipe-or-the-directive

## Material Icons
I have not found any official npm-package, Google suggest to use their cloud-hosted version. 
But some browsers do not download external files, instead the placeholders gets displayed (eg. double_arrow).   
Compromise: Self hosted - Directly Downloaded the iconfont(woff2) from https://fonts.googleapis.com/icon?family=Material+Icons and added it to `assets/iconfonts`.

https://github.com/google/material-design-icons/tree/master/iconfont
For future reference (multiple variants):  
`https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp`

## Build

To build the SPA run:

> `$ ./build.sh`

## PACKAGE as an Electron-Application:

A single bundle for the (current) host platform/architecture will be created. To target a specific 
platform/arch please refer to: https://github.com/electron/electron-packager#usage

> `$ ./build-electron.sh`

Output folder: `./electron-build/`  
The script also tries to zip the output-folder via JDK/jar.
(currently static filename, yours may vary depending on arch/platform and thus not work)

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use 
`ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the 
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## User Testing

https://www.youtube.com/watch?v=v8JJrDvQDF4

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.
