# NOTE: In case of `Permission denied`-Error, try:
#
#        chmod +x build.sh && ./build.sh
#
# -----------------------------------------------

# Exit immediately upon error, to not waste any build-time
set -e

npm run postinstall
echo "Build Angular project for Netlify..."
npm run build

echo "compress output-files (gzip)..."
gzip -krf6 dist

echo "Skipping brotli, not supported by Netlify at the moment."
#echo "compress output-files (brotli)..."
#npm run brotli
