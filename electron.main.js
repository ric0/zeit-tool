const { app, BrowserWindow, shell } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

if(process.platform === 'win32') {
  /* 
    Change the location of some (cache)-folders, 
    to prevent them from being saved in the users-roaming profile. %APPDATA%

    https://electronjs.org/docs/api/app#appgetpathname
    https://electronjs.org/docs/api/app#appsetpathname-path
  */

  const folderPath = 'C:/tmp/zeit-tool';
  app.setPath('userData', folderPath);
  app.setPath('appData', folderPath);
  app.setPath('logs', folderPath);
} 

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 600,
    height: 600,
    autoHideMenuBar: true,
    icon: "src/assets/icons/favicon-64x64.ico",
    webPreferences: {
      nodeIntegration: true,
      devTools: true,
      defaultEncoding: 'UTF-8',
    },
    show: false
  });

  // and load the index.html of the app.
  win.loadFile(`./dist/zeit-tool/index.html`);

  win.once('ready-to-show', () => {
    win.show()
  });

  win.webContents.on('new-window', function(e, url) {
    if(url.startsWith('http')) {
      /*  Open external links in Browser  */
      e.preventDefault();
      shell.openExternal(url);
    }
  });

  /* 
    Open the DevTools. DevTools have to be enabled.
    For debugging purposes.
   */
  //win.webContents.openDevTools();

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
