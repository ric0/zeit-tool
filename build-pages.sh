# Exit immediately upon error
set -e

echo "Build Angular project for Gitlab-Pages..."
npm run build-pages

echo "Copy custom 404.html into output folder..."
cp ./src/404/404.html ./public

echo "gzip output-files..."
gzip -krf6 public
