import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { Observable, timer } from 'rxjs';
import { QuotesService } from '../modules/quotes/services/quotes.service';
import { TimeService } from './time.service';
import { TimeConstants } from '../shared/constants/time-constants';

@Injectable({
  providedIn: 'root'
})
export class UpdateTimerService {
  /**
   * Update Time := 5:00 AM
   * on the next day
   */
  private readonly updateTime: DateTime = DateTime.local()
    .plus({ day: 1 })
    .set({ hour: 5, minute: 0, second: 0 }); // 5:00 AM


  constructor(
    private time: TimeService,
    private quotes: QuotesService
  ) {
    const update$ = this.createTimer();
    update$.subscribe(() => {
      this.time.updateValues();
      this.quotes.next();
    });
  }

  /**
   * create a RXJS-timer instance,
   * the timer will start after a specified dueTime,
   * and will emit values after a specified interval
   */
  private createTimer(): Observable<number> {
    const dueTime = this.updateTime.diffNow().valueOf();
    const interval = TimeConstants.ONE_DAY.valueOf();

    return timer(dueTime, interval);
  }
}
