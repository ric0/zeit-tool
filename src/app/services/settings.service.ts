import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { DEFAULT_SETTINGS } from '../shared/constants/default-settings';
import { Settings } from '../shared/models/settings.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private settings: Settings;
  values$: BehaviorSubject<Settings>;
  reset$ = new Subject<{ clearCache: boolean }>();

  constructor(private localStorage: LocalStorageService) {
    this.settings = this.localStorage.loadSettings();
    this.values$ = new BehaviorSubject<Settings>(this.settings);
  }

  getValues(): Settings {
    return this.settings;
  }

  update(values: Settings) {
    this.settings = values;
    this.localStorage.saveSettings(values);
    this.values$.next(this.settings);
  }

  reset(opts: { clearCache: boolean } = { clearCache: false }) {
    this.update(DEFAULT_SETTINGS);
    this.reset$.next(opts);
  }

  clearLocalStorage() {
    this.localStorage.clear();
    this.reset({ clearCache: true });
  }
}
