import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { BehaviorSubject } from 'rxjs';
import { TimeConstants } from '../shared/constants/time-constants';
import { InputModel, resetDayMonthYear } from '../shared/models/input.model';
import { OutputModel } from '../shared/models/output.model';
import { CalculationService } from './calculation.service';
import { LocalStorageService } from './local-storage.service';
import { SettingsService } from './settings.service';
import { DEFAULT_INPUT_VALUES } from '../shared/constants/default-input';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  output$: BehaviorSubject<OutputModel>;
  private lastInput: InputModel;

  constructor(
    private localStorage: LocalStorageService,
    private settings: SettingsService,
    private calc: CalculationService
  ) {
    const initialInput = this.localStorage.loadInput();
    if (this.settings.getValues().enableAutomaticArrivalTime) {
      initialInput.arrivalTime = DateTime.local();
    }
    const initialOutput = this.calc.calculate(initialInput);
    this.output$ = new BehaviorSubject<OutputModel>(initialOutput);

    /** UPDATE VALUES WHEN SETTINGS CHANGE */
    this.lastInput = initialInput;
    this.settings.values$.subscribe(() => {
      this.updateValues();
    });
  }

  updateValues(input: InputModel = this.lastInput) {
    input = resetDayMonthYear(input);
    const correctedInput: InputModel = this.correctInput(input);
    this.localStorage.saveInput(correctedInput);

    this.lastInput = correctedInput;

    const output = this.calc.calculate(correctedInput);
    this.output$.next(output);
  }

  resetValues() {
    this.updateValues(DEFAULT_INPUT_VALUES);
  }

  /**
   * Around Midnight the dates (of arrival and leave) have to be changed
   * in order to get correct calculation-results
   */
  private correctInput(input: InputModel): InputModel {
    if (!this.isSpanningMidnight(input.arrivalTime)) {
      return input;
    }

    return {
      ...input,
      arrivalTime: this.correctArrival(input.arrivalTime),
      leaveTime: this.correctLeave(input.arrivalTime, input.leaveTime)
    };
  }

  private correctArrival(arrival: DateTime): DateTime {
    if (this.isNowBeforeMidnight(arrival)) {
      return arrival;
    } else {
      return arrival.minus(TimeConstants.ONE_DAY);
    }
  }

  private correctLeave(arrival: DateTime, leave: DateTime): DateTime {
    if (!leave) {
      return null;
    }
    const isLeaveBeforeMidnight = this.isLeaveBeforeMidnight(arrival, leave);

    if (this.isNowBeforeMidnight(arrival)) {
      return isLeaveBeforeMidnight ? leave : leave.plus(TimeConstants.ONE_DAY);
    } else {
      return isLeaveBeforeMidnight ? leave.minus(TimeConstants.ONE_DAY) : leave;
    }
  }

  private isSpanningMidnight(arrival: DateTime) {
    const leave = arrival.plus(TimeConstants.TEN_HOURS_FORTYFIVE_MINS);

    const arr = arrival.hour * 60 + arrival.minute;
    const lea = leave.hour * 60 + leave.minute;
    return arr > lea;
  }

  private isNowBeforeMidnight(arrival: DateTime): boolean {
    const now = DateTime.local();
    const endOfDay = arrival.endOf('day');

    return arrival <= now && now <= endOfDay;
  }

  private isLeaveBeforeMidnight(arrival: DateTime, leave: DateTime): boolean {
    const endOfDay = arrival.endOf('day');

    return arrival <= leave && leave <= endOfDay;
  }
}
