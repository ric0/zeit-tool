import { TestBed } from '@angular/core/testing';
import { DateTime, Duration, Settings } from 'luxon';
import { TimeSelection } from '../shared/enums/time-selection.enum';
import { OutputModel } from '../shared/models/output.model';
import { SettingsService } from './settings.service';
import { TimeService } from './time.service';
import { skip } from 'rxjs/operators';
import { InputModel } from '../shared/models/input.model';

const defaultInput: InputModel = {
  arrivalTime: DateTime.fromFormat('06:00', 'H:mm'),
  timeSelection: null,
  workingTime: null,
  leaveTime: DateTime.local(),
  break: {
    selected: false,
    breaks: []
  }
};
let defaultNowFn: () => number;

function mockCurrentDateTimeBeforeMidnight() {
  // Used for mocking DateTime.local() ~ now
  Settings.now = () => new Date(2018, 4, 17, 23, 30).valueOf(); // '2018-5-17 - 23:30:00'
}

function mockCurrentDateTimeAfterMidnight() {
  // Used for mocking DateTime.local() ~ now
  Settings.now = () => new Date(2018, 4, 17, 1, 30).valueOf(); // '2018-5-17 - 01:30:00'
}

describe('TimeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimeService = TestBed.inject(TimeService);
    expect(service).toBeTruthy();
  });
});

describe('TimeService ', () => {
  /**
   * Some of the following tests mock the Settings.now function (from Luxon),
   * which is used to create new dates. In order for the other tests to
   * function correctly, we save the default Settings.now and reapply it again.
   */
  beforeEach(() => {
    defaultNowFn = Settings.now;

    const service: SettingsService = TestBed.inject(SettingsService);
    service.update({
      defaultWorkingTime: Duration.fromObject({ hours: 8 }),
      enableAutomaticArrivalTime: false,
      enableTabCloseWarning: false,
      quotes: null
    });
  });
  afterEach(() => {
    Settings.now = defaultNowFn;
  });

  it('should mock Date.now()', () => {
    mockCurrentDateTimeBeforeMidnight();
    expect(DateTime.local().toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual(
      '17.05.2018-23:30:00'
    );

    mockCurrentDateTimeAfterMidnight();
    expect(DateTime.local().toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual(
      '17.05.2018-01:30:00'
    );
  });

  it('should alter the date of the leavetime / before Midnight, defaultWorkingTime', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeBeforeMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.LL.yyyy-HH:mm:ss')).toEqual('17.05.2018-22:00:00');
        expect(leaveTime.toFormat('dd.LL.yyyy-HH:mm:ss')).toEqual('18.05.2018-06:30:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 22, minute: 0 }),
      timeSelection: TimeSelection.DefaultWorkingTime
    });
  });

  it('should alter the date of the arrivalTime / after Midnight, defaultWorkingTime', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeAfterMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('16.05.2018-22:00:00');
        expect(leaveTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('17.05.2018-06:30:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 22, minute: 0 }),
      timeSelection: TimeSelection.DefaultWorkingTime
    });
  });

  it('should alter the date of the arrivalTime / before Midnight, custom leaveTime', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeBeforeMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('17.05.2018-22:00:00');
        expect(leaveTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('18.05.2018-04:00:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 22, minute: 0 }),
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.fromObject({ hour: 4, minute: 0 })
    });
  });

  it('should alter the date of the arrivalTime / after Midnight, custom leaveTime', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeAfterMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('16.05.2018-22:00:00');
        expect(leaveTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('17.05.2018-04:00:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 22, minute: 0 }),
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.fromObject({ hour: 4, minute: 0 })
    });
  });

  it('should alter the date of the arrivalTime / before Midnight, leave before Midnight', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeBeforeMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('17.05.2018-16:00:00');
        expect(leaveTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('17.05.2018-23:00:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 16, minute: 0 }),
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.fromObject({ hour: 23, minute: 0 })
    });
  });

  it('should alter the date of the arrivalTime / after Midnight, leave before Midnight', () => {
    const service: TimeService = TestBed.inject(TimeService);
    mockCurrentDateTimeAfterMidnight();

    service.output$
      .pipe(
        skip(1) // BehaviourSubject: skip initial Values
      )
      .subscribe(({ arrivalTime, leaveTime }: OutputModel) => {
        expect(arrivalTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('16.05.2018-16:00:00');
        expect(leaveTime.toFormat('dd.MM.yyyy-HH:mm:ss')).toEqual('16.05.2018-23:00:00');
      });

    service.updateValues({
      ...defaultInput,
      arrivalTime: DateTime.fromObject({ hour: 16, minute: 0 }),
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.fromObject({ hour: 23, minute: 0 })
    });
  });
});
