import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  status$ = new BehaviorSubject<NotificationPermission>(
    ('Notification' in window) ? Notification.permission : 'denied'
  );
  private NOTIFICATION_ICON = '/assets/icons/notification-256x256.png';

  constructor() {
  }

  requestPermission() {
    if (!Notification || Notification.permission === 'granted') {
      return;
    }

    Notification.requestPermission().then((res: NotificationPermission) => {
      this.status$.next(res);
    });
  }

  show(title: string, body: string) {
    const options: NotificationOptions = {
      body,
      icon: this.NOTIFICATION_ICON,
      vibrate: [100, 100, 100],
      requireInteraction: true
    };

    const myNotification = new Notification(title, options); // displays Notifications automatically
    myNotification.onclick = () => myNotification.close();
  }
}
