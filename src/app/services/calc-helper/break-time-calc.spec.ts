import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { TimeSelection } from '../../shared/enums/time-selection.enum';
import { BreakTime } from '../../shared/models/break-time';
import { InputModel } from '../../shared/models/input.model';
import { BreakTimeCalc } from './break-time-calc';
import { BreakInputData, BreakInputType } from '../../shared/models/break-input.model';

const defaultInput: InputModel = {
  arrivalTime: DateTime.fromFormat('06:00', 'H:mm'),
  timeSelection: null,
  workingTime: null,
  leaveTime: null,
  break: {
    selected: false,
    breaks: null
  }
};

function assert(result: BreakTime, expected: BreakTime) {
  expect(result.required.valueOf()).toEqual(expected.required.valueOf());
  expect(result.additional.valueOf()).toEqual(expected.additional.valueOf());
  expect(result.total.valueOf()).toEqual(expected.total.valueOf());
}

describe('BreakTimeCalc should calculate correct value for defaultWorkingTime for ', () => {
  function testWrapper(defaultWorkingTime: Duration, expected: BreakTime) {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.DefaultWorkingTime
    };

    const result = calc.execute(input, defaultWorkingTime);
    assert(result, expected);
  }

  it('5h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 5, minutes: 59 }), {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    });
  });

  it('6h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 6, minutes: 0 }), {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    });
  });

  it('6h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 6, minutes: 1 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('7h 48 min', () => {
    testWrapper(Duration.fromObject({ hours: 7, minutes: 48 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('8h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 8 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('8h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 8, minutes: 59 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('9h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 0 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('9h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 1 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('9h  59 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 59 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('10h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 10, minutes: 0 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('10h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 10, minutes: 1 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });
});

describe('BreakTimeCalc should calculate correct value for given workingTime for ', () => {
  function testWrapper(workingTime: Duration, expected: BreakTime) {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.WorkingTime,
      workingTime
    };

    const result = calc.execute(input, null);
    assert(result, expected);
  }

  it('5h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 5, minutes: 59 }), {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    });
  });

  it('6h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 6, minutes: 0 }), {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    });
  });

  it('6h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 6, minutes: 1 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('8h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 8, minutes: 0 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('8h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 8, minutes: 1 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('8h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 8, minutes: 59 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('9h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 0 }), {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    });
  });

  it('9h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 1 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('9h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 59 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('10h 0 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 59 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });

  it('10h 1 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 59 }), {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    });
  });
});

describe('BreakTimeCalc should calculate correct value for leaveTime ', () => {
  function calcWrapper(leaveTime: DateTime) {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.LeaveTime,
      leaveTime
    };

    return calc.execute(input, null);
  }

  it('6:01 - same as arrival', () => {
    const result = calcWrapper(DateTime.fromFormat('6:01', 'H:mm'));
    const expected: BreakTime = {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    };
    assert(result, expected);
  });

  it('12:00', () => {
    const result = calcWrapper(DateTime.fromFormat('12:00', 'H:mm'));
    const expected = {
      required: TimeConstants.ZERO_SECONDS,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.ZERO_SECONDS
    };
    assert(result, expected);
  });

  it('12:01', () => {
    const result = calcWrapper(DateTime.fromFormat('12:01', 'H:mm'));
    const expected = {
      required: Duration.fromObject({ seconds: 60 }),
      additional: TimeConstants.ZERO_SECONDS,
      total: Duration.fromObject({ seconds: 60 })
    };
    assert(result, expected);
  });

  it('12:29', () => {
    const result = calcWrapper(DateTime.fromFormat('12:29', 'H:mm'));
    const expected = {
      required: Duration.fromObject({ minutes: 29 }),
      additional: TimeConstants.ZERO_SECONDS,
      total: Duration.fromObject({ minutes: 29 })
    };
    assert(result, expected);
  });

  it('12:30', () => {
    const result = calcWrapper(DateTime.fromFormat('12:30', 'H:mm'));
    const expected = {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    };
    assert(result, expected);
  });

  it('12:31', () => {
    const result = calcWrapper(DateTime.fromFormat('12:31', 'H:mm'));
    const expected = {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    };
    assert(result, expected);
  });

  it('15:30', () => {
    const result = calcWrapper(DateTime.fromFormat('15:30', 'H:mm'));
    const expected = {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.THIRTY_MINUTES
    };
    assert(result, expected);
  });

  it('15:31', () => {
    const result = calcWrapper(DateTime.fromFormat('15:31', 'H:mm'));
    const expected = {
      required: Duration.fromObject({ minutes: 31 }),
      additional: TimeConstants.ZERO_SECONDS,
      total: Duration.fromObject({ minutes: 31 })
    };
    assert(result, expected);
  });

  it('15:45', () => {
    const result = calcWrapper(DateTime.fromFormat('15:45', 'H:mm'));
    const expected = {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    };
    assert(result, expected);
  });

  it('15:46', () => {
    const result = calcWrapper(DateTime.fromFormat('15:46', 'H:mm'));
    const expected = {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    };
    assert(result, expected);
  });

  it('18:00', () => {
    const result = calcWrapper(DateTime.fromFormat('18:00', 'H:mm'));
    const expected = {
      required: TimeConstants.FORTY_FIVE_MINUTES,
      additional: TimeConstants.ZERO_SECONDS,
      total: TimeConstants.FORTY_FIVE_MINUTES
    };
    assert(result, expected);
  });
});

describe(
  'BreakTimeCalc should calculate correct value for given breaktime ' +
  'for workingTime [t <= 6h] => no break required',
  () => {
    function testWrapper(breakTime: Duration, expected: BreakTime) {
      const calc = new BreakTimeCalc();
      const input: InputModel = {
        ...defaultInput,
        timeSelection: TimeSelection.WorkingTime,
        workingTime: Duration.fromObject({ hours: 6, minutes: 0 }),
        break: {
          selected: true,
          breaks: [{ type: BreakInputType.DURATION, data: breakTime }]
        }
      };

      const result = calc.execute(input, null);
      assert(result, expected);
    }

    it('0h 0 min', () => {
      testWrapper(TimeConstants.ZERO_SECONDS, {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.ZERO_SECONDS
      });
    });

    it('0h 1 min', () => {
      testWrapper(Duration.fromObject({ minutes: 1 }), {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 1 }),
        total: Duration.fromObject({ minutes: 1 })
      });
    });

    it('1h 0 min', () => {
      testWrapper(Duration.fromObject({ hours: 1 }), {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ hours: 1 }),
        total: Duration.fromObject({ hours: 1 })
      });
    });

    it('4h 2 min', () => {
      testWrapper(Duration.fromObject({ hours: 4, minutes: 2 }), {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ hours: 4, minutes: 2 }),
        total: Duration.fromObject({ hours: 4, minutes: 2 })
      });
    });
  }
);

describe(
  'BreakTimeCalc should calculate correct value for given breaktime ' +
  'for workingTime [6h < t < 9h] => 30min break required',
  () => {
    function testWrapper(breakTime: Duration, expected: BreakTime) {
      const calc = new BreakTimeCalc();
      const input: InputModel = {
        ...defaultInput,
        timeSelection: TimeSelection.WorkingTime,
        workingTime: Duration.fromObject({ hours: 8, minutes: 0 }),
        break: {
          selected: true,
          breaks: [{ type: BreakInputType.DURATION, data: breakTime }]
        }
      };

      const result = calc.execute(input, null);

      assert(result, expected);
    }

    it('0h 0 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 0 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.THIRTY_MINUTES
      });
    });

    it('0h  1 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 1 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 1 }),
        total: Duration.fromObject({ minutes: 31 })
      });
    });

    it('0h 29 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 29 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.THIRTY_MINUTES
      });
    });

    it('0h 30 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 30 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.THIRTY_MINUTES
      });
    });

    it('0h 31 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 31 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 1 }),
        total: Duration.fromObject({ minutes: 31 })
      });
    });

    it('1h 0 min', () => {
      testWrapper(Duration.fromObject({ hours: 1, minutes: 0 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 30 }),
        total: Duration.fromObject({ minutes: 60 })
      });
    });

    it('4h 2 min', () => {
      testWrapper(Duration.fromObject({ hours: 4, minutes: 2 }), {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ hours: 3, minutes: 32 }),
        total: Duration.fromObject({ hours: 4, minutes: 2 })
      });
    });
  }
);

describe(
  'BreakTimeCalc should calculate correct value for given breaktime ' +
  'for workingTime [9h < t] => 45min break required',
  () => {
    function testWrapper(breakTime: Duration, expected: BreakTime) {
      const calc = new BreakTimeCalc();
      const input: InputModel = {
        ...defaultInput,
        timeSelection: TimeSelection.WorkingTime,
        workingTime: Duration.fromObject({ hours: 9, minutes: 30 }),
        break: {
          selected: true,
          breaks: [{ type: BreakInputType.DURATION, data: breakTime }]
        }
      };

      const result = calc.execute(input, null);
      assert(result, expected);
    }

    it('0h 0 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 0 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.FORTY_FIVE_MINUTES
      });
    });

    it('0h 25 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 25 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.FORTY_FIVE_MINUTES
      });
    });

    it('0h 44 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 44 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.FORTY_FIVE_MINUTES
      });
    });

    it('0h 45 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 45 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: TimeConstants.FORTY_FIVE_MINUTES
      });
    });

    it('0h 46 min', () => {
      testWrapper(Duration.fromObject({ hours: 0, minutes: 46 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 0, minutes: 1 }),
        total: Duration.fromObject({ minutes: 46 })
      });
    });

    it('4h  2 min', () => {
      testWrapper(Duration.fromObject({ hours: 4, minutes: 2 }), {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 3, minutes: 17 }),
        total: Duration.fromObject({ hours: 4, minutes: 2 })
      });
    });
  }
);

describe('BreakTimeCalc should calculate correct value for a combination of inputs ', () => {
  it('defaultWorkingtime and custom Breaktime', () => {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.DefaultWorkingTime,
      break: {
        selected: true,
        breaks: [
          {
            type: BreakInputType.DURATION,
            data: Duration.fromObject({ minutes: 90 })
          }
        ]
      }
    };
    const expected: BreakTime = {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 60 }),
      total: Duration.fromObject({ hours: 1, minutes: 30 })
    };
    const defaultWorkingTime = Duration.fromObject({ hours: 8 });

    const result = calc.execute(input, defaultWorkingTime);
    assert(result, expected);
  });

  it('custom workingtime and custom Breaktime', () => {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.WorkingTime,
      workingTime: Duration.fromObject({ hours: 3 }),
      break: {
        selected: true,
        breaks: [
          {
            type: BreakInputType.DURATION,
            data: Duration.fromObject({ minutes: 90 })
          }
        ]
      }
    };
    const expected: BreakTime = {
      required: TimeConstants.ZERO_SECONDS,
      additional: Duration.fromObject({ minutes: 90 }),
      total: Duration.fromObject({ hours: 2 })
    };

    const result = calc.execute(input, null);
    expect(result.required.valueOf()).toEqual(expected.required.valueOf());
    expect(result.additional.valueOf()).toEqual(expected.additional.valueOf());
  });

  it('custom leaveTime and custom Breaktime', () => {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.fromFormat('12:30', 'H:mm'),
      break: {
        selected: true,
        breaks: [
          {
            type: BreakInputType.DURATION,
            data: Duration.fromObject({ minutes: 80 })
          }
        ]
      }
    };
    const expected: BreakTime = {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 50 }),
      total: Duration.fromObject({ hours: 1, minutes: 20 })
    };

    const result = calc.execute(input, null);
    expect(result.required.valueOf()).toEqual(expected.required.valueOf());
    expect(result.additional.valueOf()).toEqual(expected.additional.valueOf());
  });
});

describe('BreakTimeCalc should calculate correct value for', () => {
  function testWrapper(breaks: BreakInputData[], expected: BreakTime) {
    const calc = new BreakTimeCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.WorkingTime,
      workingTime: Duration.fromObject({ hours: 8, minutes: 0 }),
      break: {
        selected: true,
        breaks
      }
    };

    const result = calc.execute(input, null);
    assert(result, expected);
  }

  it('multiple break durations, each more or equal than 15min', () => {
    testWrapper([{
      type: BreakInputType.DURATION,
      data: TimeConstants.FIFTEEN_MINUTES
    }, {
      type: BreakInputType.DURATION,
      data: TimeConstants.FORTY_FIVE_MINUTES
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: TimeConstants.THIRTY_MINUTES,
      total: TimeConstants.ONE_HOUR
    });
  });

  it('multiple break durations, each less than 15min', () => {
    testWrapper([{
      type: BreakInputType.DURATION,
      data: Duration.fromObject({ minutes: 10 })
    }, {
      type: BreakInputType.DURATION,
      data: Duration.fromObject({ minutes: 4 })
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 14 }),
      total: Duration.fromObject({ minutes: 44 })
    });
  });

  it('multiple time ranges, each more or equal than 15min', () => {
    const now = DateTime.local();
    testWrapper([{
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 10, minute: 33 }),
        end: now.set({ hour: 11, minute: 3 })
        // -> 30 minutes
      }
    }, {
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 14, minute: 55 }),
        end: now.set({ hour: 15, minute: 17 })
        // -> 22 minutes
      }
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 22 }),
      total: Duration.fromObject({ minutes: 52 })
    });
  });

  it('multiple time ranges, each less than 15min', () => {
    const now = DateTime.local();
    testWrapper([{
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 10, minute: 33 }),
        end: now.set({ hour: 10, minute: 37 })
        // -> 4 minutes
      }
    }, {
      type: BreakInputType.RANGE,
      data: {
        begin: DateTime.local().set({ hour: 14, minute: 55 }),
        end: DateTime.local().set({ hour: 15, minute: 9 })
        // -> 14 minutes
      }
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 18 }),
      total: Duration.fromObject({ minutes: 48 })
    });
  });

  it('a combination of durations and time ranges, each more or equal than 15min', () => {
    const now = DateTime.local();
    testWrapper([{
      type: BreakInputType.DURATION,
      data: Duration.fromObject({ minutes: 21 })
    }, {
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 11, minute: 15 }),
        end: now.set({ hour: 11, minute: 35 })
        // -> 20 minutes
      }
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 11 }),
      total: Duration.fromObject({ minutes: 41 })
    });
  });

  it('a combination of durations and time ranges, each less than 15min', () => {
    const now = DateTime.local();
    testWrapper([{
      type: BreakInputType.DURATION,
      data: Duration.fromObject({ minutes: 1 })
    }, {
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 11, minute: 15 }),
        end: now.set({ hour: 11, minute: 17 })
        // -> 2 minutes
      }
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 3 }),
      total: Duration.fromObject({ minutes: 33 })
    });
  });

  it('a combination of durations and time ranges, one less than 15min another more than 15min', () => {
    const now = DateTime.local();
    testWrapper([{
      type: BreakInputType.DURATION,
      data: Duration.fromObject({ minutes: 33 })
    }, {
      type: BreakInputType.RANGE,
      data: {
        begin: now.set({ hour: 11, minute: 15 }),
        end: now.set({ hour: 11, minute: 29 })
        // -> 14 minutes
      }
    }], {
      required: TimeConstants.THIRTY_MINUTES,
      additional: Duration.fromObject({ minutes: 17 }),
      total: Duration.fromObject({ minutes: 47 })
    });
  });
});
