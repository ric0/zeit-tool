import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { TimeSelection } from '../../shared/enums/time-selection.enum';
import { BreakTime } from '../../shared/models/break-time';
import { InputModel } from '../../shared/models/input.model';
import { BreakInputModel, extractEligibleBreakSum, extractIneligibleBreakSum } from '../../shared/models/break-input.model';

export class BreakTimeCalc {

  /**
   * Calculates the BreakTime based either on:
   *  - defaultWorkingTime (from settings.service)
   *  - given workingTime from user-input
   *  - given leaveTime from user-input
   *
   * And if selected a custom Breaktime
   */
  execute(input: InputModel, defaultWorkingTime: Duration): BreakTime {
    const required = this.calcRequiredBreakTime(input, defaultWorkingTime);
    const additional = this.calcAdditionalBreakTime(input.break, required);
    const total = required.plus(additional);

    return { required, additional, total };
  }

  private calcRequiredBreakTime(
    input: InputModel,
    defaultWorkingTime: Duration
  ): Duration {
    switch (input.timeSelection) {
      case TimeSelection.DefaultWorkingTime:
        return this.calcBreakTimeBasedOnWorkingTime(defaultWorkingTime);

      case TimeSelection.WorkingTime:
        return this.calcBreakTimeBasedOnWorkingTime(input.workingTime);

      case TimeSelection.LeaveTime:
        return this.calcBreakTimeBasedOnLeaveTime(
          input.arrivalTime,
          input.leaveTime
        );

      default:
        console.error(`Invalid TimeSelection: ${input.timeSelection}`);
        return TimeConstants.ZERO_SECONDS;
    }
  }

  private calcBreakTimeBasedOnWorkingTime(workingTime: Duration): Duration {
    let breakTime: Duration;

    if (workingTime > TimeConstants.NINE_HOURS) {
      breakTime = TimeConstants.FORTY_FIVE_MINUTES;
    } else if (workingTime > TimeConstants.SIX_HOURS) {
      breakTime = TimeConstants.THIRTY_MINUTES;
    } else {
      breakTime = TimeConstants.ZERO_SECONDS;
    }

    return breakTime;
  }

  private calcBreakTimeBasedOnLeaveTime(arrivalTime: DateTime, leaveTime: DateTime): Duration {
    const workingTimeWithBreak: Duration = leaveTime.diff(arrivalTime).normalize();
    return this.calcBreakTimeBasedOnAttendance(workingTimeWithBreak);
  }

  /**
   * gesetzliche Vorgabe: (Stand: Juni 2019)
   *
   * |    Arbeit   | Pause | Arbeit | Pause | Arbeit |
   * |     6h      | 30min |   3h   | 15min |   1h   |
   *
   *  => max Arbeitszeit: 10h
   *  => max Pause:       45min
   *
   * @param attendanceTime in der man anwesend ist, incl. Pause
   * @returns breakTime
   */
  private calcBreakTimeBasedOnAttendance(attendanceTime: Duration): Duration {
    let remaining = attendanceTime;
    let breakTime: Duration = TimeConstants.ZERO_SECONDS;

    if (remaining <= TimeConstants.SIX_HOURS) {
      breakTime = TimeConstants.ZERO_SECONDS;
    } else {
      remaining = remaining.minus(TimeConstants.SIX_HOURS);

      if (remaining <= TimeConstants.THIRTY_MINUTES) {
        breakTime = remaining;
      } else {
        breakTime = breakTime.plus(TimeConstants.THIRTY_MINUTES);
        remaining = remaining.minus(TimeConstants.THIRTY_MINUTES);

        if (remaining.valueOf() > TimeConstants.THREE_HOURS.valueOf()) {
          remaining = remaining.minus(TimeConstants.THREE_HOURS);

          if (remaining <= TimeConstants.FIFTEEN_MINUTES) {
            breakTime = breakTime.plus(remaining);
          } else {
            breakTime = breakTime.plus(TimeConstants.FIFTEEN_MINUTES);
          }
        }
      }
    }

    return breakTime;
  }

  private calcAdditionalBreakTime(b: BreakInputModel, required: Duration): Duration {
    if (!b.selected) {
      return TimeConstants.ZERO_SECONDS;
    }

    const eligibleBreak = extractEligibleBreakSum(b);
    const ineligibleBreak = extractIneligibleBreakSum(b);

    let res;
    if (eligibleBreak > required) {
      res = eligibleBreak.minus(required);
    } else {
      res = TimeConstants.ZERO_SECONDS;
    }
    return res.plus(ineligibleBreak);
  }
}
