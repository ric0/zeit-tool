import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { TimeSelection } from '../../shared/enums/time-selection.enum';
import { BreakTime } from '../../shared/models/break-time';
import { InputModel } from '../../shared/models/input.model';

export interface WorkingAndLeaveTime {
  workingTime: Duration;
  leaveTime: DateTime;
}

export class WorkingOrLeaveCalc {
  constructor() {}

  execute(input: InputModel, breakTime: BreakTime, defaultWorkingTime: Duration): WorkingAndLeaveTime {
    const timeSelection: TimeSelection = input.timeSelection;

    let workingTime: Duration;
    let leaveTime: DateTime;

    switch (timeSelection) {
      case TimeSelection.DefaultWorkingTime:
        workingTime = this.checkTenHourLimit(defaultWorkingTime);
        leaveTime = this.calcLeaveTime(input.arrivalTime, workingTime, breakTime);
        break;

      case TimeSelection.WorkingTime:
        workingTime = this.checkTenHourLimit(input.workingTime);
        leaveTime = this.calcLeaveTime(input.arrivalTime, workingTime, breakTime);
        break;

      case TimeSelection.LeaveTime:
        workingTime = this.calcWorkingTime(input.arrivalTime, input.leaveTime, breakTime);
        workingTime = this.checkTenHourLimit(workingTime);
        leaveTime = input.leaveTime;
        break;

      default:
        throw new Error(`Unknown value for TimeSelection: ${timeSelection}`);
    }

    return { workingTime, leaveTime };
  }

  private calcLeaveTime(arrival: DateTime, working: Duration, breakTime: BreakTime): DateTime {
    return arrival
      .plus(working)
      .plus(breakTime.required)
      .plus(breakTime.additional);
  }

  private calcWorkingTime(arrival: DateTime, leave: DateTime, breakTime: BreakTime): Duration {
    const attendance = leave.diff(arrival);
    const workingTime = attendance
      .minus(breakTime.required)
      .minus(breakTime.additional);

    return workingTime.valueOf() < 0
      ? TimeConstants.ZERO_SECONDS
      : workingTime;
  }

  private checkTenHourLimit(dur: Duration): Duration {
    return dur.valueOf() > TimeConstants.TEN_HOURS.valueOf()
      ? TimeConstants.TEN_HOURS
      : dur;
  }
}
