import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { TimeSelection } from '../../shared/enums/time-selection.enum';
import { BreakTime } from '../../shared/models/break-time';
import { InputModel } from '../../shared/models/input.model';
import { WorkingAndLeaveTime, WorkingOrLeaveCalc } from './working-or-leave-calc';

const defaultInput: InputModel = {
  arrivalTime: DateTime.fromFormat('6:00', 'H:mm'),
  timeSelection: null,
  workingTime: null,
  leaveTime: null,
  break: {
    selected: false
  }
};

describe('WorkingOrLeaveCalc should calculate correct values for DefaultWorkingTime-Selection with different BreakTimes ', () => {
  function testWrapper(
    defaultWorkingTime: Duration,
    breakTime: BreakTime,
    expected: WorkingAndLeaveTime
  ) {
    const calc = new WorkingOrLeaveCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.DefaultWorkingTime
    };
    const result = calc.execute(input, breakTime, defaultWorkingTime);
    expect(result.workingTime.valueOf()).toEqual(
      expected.workingTime.valueOf()
    );
    expect(result.leaveTime.valueOf()).toEqual(expected.leaveTime.valueOf());
  }

  it('work: 5h, break: 0 min + 0min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('11:00', 'H:mm')
      }
    );
  });

  it('work: 5h, break: 0 min + 30min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 30 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('11:30', 'H:mm')
      }
    );
  });

  it('work: 5h, break: 0 min + 1h 42min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('12:42', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min - 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('14:30', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min + 30 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 30 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('15:00', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('16:12', 'H:mm')
      }
    );
  });

  it('work: 9h, break: 30 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('15:30', 'H:mm')
      }
    );
  });

  it('work: 9h, break: 30 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('17:12', 'H:mm')
      }
    );
  });

  it('work: 9h 30 min, break: 45min + 0 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9, minutes: 30 }),
        leaveTime: DateTime.fromFormat('16:15', 'H:mm')
      }
    );
  });

  it('work: 9h 30 min, break: 45min + 1 min + 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9, minutes: 30 }),
        leaveTime: DateTime.fromFormat('17:57', 'H:mm')
      }
    );
  });

  it('work: 10h 0 min, break: 45 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 10 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:45', 'H:mm')
      }
    );
  });

  it('work: 10h 0 min, break: 45 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 10 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('18:27', 'H:mm')
      }
    );
  });

  it('work: 11h 30 min, break: 45 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 11, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:45', 'H:mm')
      }
    );
  });

  it('work: 11h 30 min, break: 45 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 11, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('18:27', 'H:mm')
      }
    );
  });
});

describe('WorkingOrLeaveCalc should calculate correct values for custom WorkingTime-Selection with different BreakTimes ', () => {
  function testWrapper(
    workingTime: Duration,
    breakTime: BreakTime,
    expected: WorkingAndLeaveTime
  ) {
    const calc = new WorkingOrLeaveCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.WorkingTime,
      workingTime
    };
    const result = calc.execute(input, breakTime, null);
    expect(result.workingTime.valueOf()).toEqual(
      expected.workingTime.valueOf()
    );
    expect(result.leaveTime.valueOf()).toEqual(expected.leaveTime.valueOf());
  }

  it('work: 5h, break: 0 min + 0min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('11:00', 'H:mm')
      }
    );
  });

  it('work: 5h, break: 0 min + 30min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 30 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('11:30', 'H:mm')
      }
    );
  });

  it('work: 5h, break: 0 min + 1h 42min', () => {
    testWrapper(
      Duration.fromObject({ hours: 5 }),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5 }),
        leaveTime: DateTime.fromFormat('12:42', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min - 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('14:30', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min + 30 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 30 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('15:00', 'H:mm')
      }
    );
  });

  it('work: 8h, break: 30 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 8 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 8 }),
        leaveTime: DateTime.fromFormat('16:12', 'H:mm')
      }
    );
  });

  it('work: 9h, break: 30 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('15:30', 'H:mm')
      }
    );
  });

  it('work: 9h, break: 30 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9 }),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('17:12', 'H:mm')
      }
    );
  });

  it('work: 9h 30 min, break: 45min + 0 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9, minutes: 30 }),
        leaveTime: DateTime.fromFormat('16:15', 'H:mm')
      }
    );
  });

  it('work: 9h 30 min, break: 45min + 1 min + 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 9, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9, minutes: 30 }),
        leaveTime: DateTime.fromFormat('17:57', 'H:mm')
      }
    );
  });

  it('work: 10h 0 min, break: 45 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 10 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:45', 'H:mm')
      }
    );
  });

  it('work: 10h 0 min, break: 45 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 10 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('18:27', 'H:mm')
      }
    );
  });

  it('work: 11h 30 min, break: 45 min + 0 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 11, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:45', 'H:mm')
      }
    );
  });

  it('work: 11h 30 min, break: 45 min + 1h 42 min', () => {
    testWrapper(
      Duration.fromObject({ hours: 11, minutes: 30 }),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: Duration.fromObject({ hours: 1, minutes: 42 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('18:27', 'H:mm')
      }
    );
  });
});

describe('WorkingOrLeaveCalc should calculate correct values for LeaveTime-Selection with different BreakTimes ', () => {
  function testWrapper(
    leaveTime: DateTime,
    breakTime: BreakTime,
    expected: WorkingAndLeaveTime
  ) {
    const calc = new WorkingOrLeaveCalc();
    const input: InputModel = {
      ...defaultInput,
      timeSelection: TimeSelection.LeaveTime,
      leaveTime
    };
    const result = calc.execute(input, breakTime, null);
    expect(result.workingTime.valueOf()).toEqual(
      expected.workingTime.valueOf()
    );
    expect(result.leaveTime.valueOf()).toEqual(expected.leaveTime.valueOf());
  }

  it('leave: 6:00, break: 0 min', () => {
    testWrapper(
      DateTime.fromFormat('6:00', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: TimeConstants.ZERO_SECONDS,
        leaveTime: DateTime.fromFormat('6:00', 'H:mm')
      }
    );
  });

  it('leave: 6:00, break: 0 min + 12min', () => {
    testWrapper(
      DateTime.fromFormat('6:00', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 12 }),
        total: null
      },
      {
        workingTime: TimeConstants.ZERO_SECONDS,
        leaveTime: DateTime.fromFormat('6:00', 'H:mm')
      }
    );
  });

  it('leave: 6:01, break: 0 min', () => {
    testWrapper(
      DateTime.fromFormat('6:01', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ minutes: 1 }),
        leaveTime: DateTime.fromFormat('6:01', 'H:mm')
      }
    );
  });

  it('leave: 11:59, break: 0 min', () => {
    testWrapper(
      DateTime.fromFormat('11:59', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5, minutes: 59 }),
        leaveTime: DateTime.fromFormat('11:59', 'H:mm')
      }
    );
  });

  it('leave: 11:59, break: 0 min + 12min', () => {
    testWrapper(
      DateTime.fromFormat('11:59', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 12 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5, minutes: 47 }),
        leaveTime: DateTime.fromFormat('11:59', 'H:mm')
      }
    );
  });

  it('leave: 12:00, break: 0 min', () => {
    testWrapper(
      DateTime.fromFormat('12:00', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 6 }),
        leaveTime: DateTime.fromFormat('12:00', 'H:mm')
      }
    );
  });

  it('leave: 12:00, break: 0 min + 12min', () => {
    testWrapper(
      DateTime.fromFormat('12:00', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 12 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5, minutes: 48 }),
        leaveTime: DateTime.fromFormat('12:00', 'H:mm')
      }
    );
  });

  it('leave: 12:01, break: 1 min', () => {
    testWrapper(
      DateTime.fromFormat('12:01', 'H:mm'),
      {
        required: Duration.fromObject({ minutes: 1 }),
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 6 }),
        leaveTime: DateTime.fromFormat('12:01', 'H:mm')
      }
    );
  });

  it('leave: 12:01, break: 0 min + 12min', () => {
    testWrapper(
      DateTime.fromFormat('12:01', 'H:mm'),
      {
        required: TimeConstants.ZERO_SECONDS,
        additional: Duration.fromObject({ minutes: 12 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 5, minutes: 49 }),
        leaveTime: DateTime.fromFormat('12:01', 'H:mm')
      }
    );
  });

  it('leave: 12:30, break: 30 min', () => {
    testWrapper(
      DateTime.fromFormat('12:30', 'H:mm'),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 6 }),
        leaveTime: DateTime.fromFormat('12:30', 'H:mm')
      }
    );
  });

  it('leave: 12:31, break: 30 min', () => {
    testWrapper(
      DateTime.fromFormat('12:31', 'H:mm'),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 6, minutes: 1 }),
        leaveTime: DateTime.fromFormat('12:31', 'H:mm')
      }
    );
  });

  it('leave: 12:31, break: 30 min + 1min', () => {
    testWrapper(
      DateTime.fromFormat('12:31', 'H:mm'),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: Duration.fromObject({ minutes: 1 }),
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 6 }),
        leaveTime: DateTime.fromFormat('12:31', 'H:mm')
      }
    );
  });

  it('leave: 15:30, break: 30 min', () => {
    testWrapper(
      DateTime.fromFormat('15:30', 'H:mm'),
      {
        required: TimeConstants.THIRTY_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('15:30', 'H:mm')
      }
    );
  });

  it('leave: 15:31, break: 31 min', () => {
    testWrapper(
      DateTime.fromFormat('15:31', 'H:mm'),
      {
        required: Duration.fromObject({ minutes: 31 }),
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('15:31', 'H:mm')
      }
    );
  });

  it('leave: 15:45, break: 45 min', () => {
    testWrapper(
      DateTime.fromFormat('15:45', 'H:mm'),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9 }),
        leaveTime: DateTime.fromFormat('15:45', 'H:mm')
      }
    );
  });

  it('leave: 15:46, break: 45 min', () => {
    testWrapper(
      DateTime.fromFormat('15:46', 'H:mm'),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 9, minutes: 1 }),
        leaveTime: DateTime.fromFormat('15:46', 'H:mm')
      }
    );
  });

  it('leave: 16:45, break: 45 min', () => {
    testWrapper(
      DateTime.fromFormat('16:45', 'H:mm'),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:45', 'H:mm')
      }
    );
  });

  it('leave: 16:46, break: 45 min', () => {
    testWrapper(
      DateTime.fromFormat('16:46', 'H:mm'),
      {
        required: TimeConstants.FORTY_FIVE_MINUTES,
        additional: TimeConstants.ZERO_SECONDS,
        total: null
      },
      {
        workingTime: Duration.fromObject({ hours: 10 }),
        leaveTime: DateTime.fromFormat('16:46', 'H:mm')
      }
    );
  });
});
