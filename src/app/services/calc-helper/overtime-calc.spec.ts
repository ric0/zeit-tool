import { Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { Balance } from '../../shared/enums/balance.enum';
import { TimeDiff } from '../../shared/models/time-diff';
import { OvertimeCalc } from './overtime-calc';

describe('OvertimeCalc should calculate correct values for workingTime ', () => {
  function testWrapper(workingTime: Duration, expected: TimeDiff) {
    const calc = new OvertimeCalc();

    const defaultWorkingTime = Duration.fromObject({ hours: 8 });
    const result: TimeDiff = calc.execute(defaultWorkingTime, workingTime);
    expect(result.balance).toEqual(expected.balance);
    expect(result.value.valueOf()).toEqual(expected.value.valueOf());
  }

  it('3h 25 min', () => {
    testWrapper(Duration.fromObject({ hours: 3, minutes: 25 }), {
      balance: Balance.Negative,
      value: Duration.fromObject({ hours: 4, minutes: 35 })
    });
  });

  it('6h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 6, minutes: 0 }), {
      balance: Balance.Negative,
      value: Duration.fromObject({ hours: 2 })
    });
  });

  it('7h 59 min', () => {
    testWrapper(Duration.fromObject({ hours: 7, minutes: 59 }), {
      balance: Balance.Negative,
      value: Duration.fromObject({ minutes: 1 })
    });
  });

  it('8h', () => {
    testWrapper(Duration.fromObject({ hours: 8 }), {
      balance: Balance.Neutral,
      value: TimeConstants.ZERO_SECONDS
    });
  });

  it('8h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 8, minutes: 1 }), {
      balance: Balance.Positive,
      value: Duration.fromObject({ minutes: 1 })
    });
  });

  it('9h  59 min', () => {
    testWrapper(Duration.fromObject({ hours: 9, minutes: 59 }), {
      balance: Balance.Positive,
      value: Duration.fromObject({ hours: 1, minutes: 59 })
    });
  });

  it('10h  0 min', () => {
    testWrapper(Duration.fromObject({ hours: 10 }), {
      balance: Balance.Positive,
      value: Duration.fromObject({ hours: 2 })
    });
  });

  it('10h  1 min', () => {
    testWrapper(Duration.fromObject({ hours: 10, minutes: 1 }), {
      balance: Balance.Positive,
      value: Duration.fromObject({ hours: 2 })
    });
  });

  it('12h  33 min', () => {
    testWrapper(Duration.fromObject({ hours: 12, minutes: 33 }), {
      balance: Balance.Positive,
      value: Duration.fromObject({ hours: 2 })
    });
  });
});
