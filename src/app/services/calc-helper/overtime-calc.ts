import { Duration } from 'luxon';
import { TimeConstants } from '../../shared/constants/time-constants';
import { Balance } from '../../shared/enums/balance.enum';
import { TimeDiff } from '../../shared/models/time-diff';
import { calcTimeDifference } from './time-diff-helper';

export class OvertimeCalc {
  constructor() {}

  /**
   * berechne die Ueberstunden basierend auf der taeglich vorgegebenen Standard Arbeitszeit
   * (durch den Nutzer aenderbar)
   */
  execute(defaultWorkingTime: Duration, currentWorkingTime: Duration): TimeDiff {
    const overtime = calcTimeDifference(currentWorkingTime, defaultWorkingTime);

    if (overtime.balance === Balance.Positive) {
      overtime.value = this.calcMaxOvertime(defaultWorkingTime, overtime.value);
    }

    return overtime;
  }

  /**
   * Ueberstunden sind limitiert durch die maximal mögliche Arbeitszeit von 10 Std. (Stand: 2019)
   * Daher werden nur die maximal möglichen Ueberstunden angezeigt
   */
  private calcMaxOvertime(defaultWorkingTime: Duration, overtime: Duration): Duration {
    const dayLimit = TimeConstants.TEN_HOURS;

    const maxOvertime: Duration = dayLimit.minus(defaultWorkingTime);

    if (maxOvertime.minus(overtime).valueOf() < 0) {
      return maxOvertime;
    } else {
      return overtime;
    }
  }
}
