import { DateTime, Duration } from 'luxon';
import { Balance } from 'src/app/shared/enums/balance.enum';
import { TimeDiff } from 'src/app/shared/models/time-diff';

export function calcTimeDifference(
  first: DateTime | Duration,
  second: DateTime | Duration
): TimeDiff {
  let balance: Balance;
  let diff: Duration;

  if (DateTime.isDateTime(first) && DateTime.isDateTime(second)) {
    diff = first.diff(second);
  } else if (Duration.isDuration(first) && Duration.isDuration(second)) {
    diff = first.minus(second);
  } else {
    diff = Duration.fromMillis(0);
  }

  if (diff.valueOf() > 0) {
    balance = Balance.Positive;
  } else if (diff.valueOf() === 0) {
    balance = Balance.Neutral;
  } else {
    balance = Balance.Negative;
    diff = diff.negate();
  }

  return {
    balance,
    value: diff.normalize()
  };
}
