import { TestBed } from '@angular/core/testing';
import { DateTime, Duration } from 'luxon';
import { TimeSelection } from '../shared/enums/time-selection.enum';
import { InputModel } from '../shared/models/input.model';
import { CalculationService } from './calculation.service';
import { SettingsService } from './settings.service';
import { BreakInputType } from '../shared/models/break-input.model';

describe('CalculationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});

    const settings: SettingsService = TestBed.inject(SettingsService);
    settings.update({
      defaultWorkingTime: Duration.fromObject({ hours: 8, minutes: 0 }),
      enableAutomaticArrivalTime: false,
      enableTabCloseWarning: false,
      quotes: null
    });
  });

  it('should be created', () => {
    const service: CalculationService = TestBed.inject(CalculationService);
    expect(service).toBeTruthy();
  });

  it('should calculate values for given input', () => {
    const service: CalculationService = TestBed.inject(CalculationService);

    const input: InputModel = {
      arrivalTime: DateTime.fromFormat('6:00', 'H:mm'),
      timeSelection: TimeSelection.WorkingTime,
      workingTime: Duration.fromObject({ hours: 9, minutes: 22 }),
      leaveTime: null,
      break: {
        selected: true,
        breaks: [
          {
            type: BreakInputType.DURATION,
            data: Duration.fromObject({ minutes: 53 })
          }
        ]
      }
    };
    const output = service.calculate(input);

    expect(output.arrivalTime.valueOf()).toBe(
      DateTime.fromFormat('6:00', 'H:mm').valueOf()
    );
    expect(output.breakTime.total.valueOf()).toBe(
      Duration.fromObject({ minutes: 53 }).valueOf()
    );
    expect(output.workingTime.valueOf()).toBe(
      Duration.fromObject({ hours: 9, minutes: 22 }).valueOf()
    );
    expect(output.leaveTime.valueOf()).toBe(
      DateTime.fromFormat('16:15', 'H:mm').valueOf()
    );
    expect(output.overtime.value.valueOf()).toBe(
      Duration.fromObject({ hours: 1, minutes: 22 }).valueOf()
    );
  });
});
