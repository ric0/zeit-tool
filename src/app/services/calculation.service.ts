import { Injectable } from '@angular/core';
import { BreakTime } from '../shared/models/break-time';
import { InputModel } from '../shared/models/input.model';
import { OutputModel } from '../shared/models/output.model';
import { TimeDiff } from '../shared/models/time-diff';
import { BreakTimeCalc } from './calc-helper/break-time-calc';
import { OvertimeCalc } from './calc-helper/overtime-calc';
import { WorkingOrLeaveCalc } from './calc-helper/working-or-leave-calc';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  constructor(private settings: SettingsService) { }

  calculate(input: InputModel): OutputModel {
    const defaultWorkingTime = this.settings.getValues().defaultWorkingTime;

    const breakTime: BreakTime = new BreakTimeCalc().execute(input, defaultWorkingTime);
    const { workingTime, leaveTime } = new WorkingOrLeaveCalc().execute(input, breakTime, defaultWorkingTime);
    const overtime: TimeDiff = new OvertimeCalc().execute(defaultWorkingTime, workingTime);

    return {
      arrivalTime: input.arrivalTime,
      workingTime,
      breakTime,
      leaveTime,
      overtime
    };
  }
}
