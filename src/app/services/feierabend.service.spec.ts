import { TestBed } from '@angular/core/testing';

import { FeierabendService } from './feierabend.service';

describe('FeierabendService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeierabendService = TestBed.inject(FeierabendService);
    expect(service).toBeTruthy();
  });
});
