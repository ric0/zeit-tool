import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { Subject, Subscription, timer } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NotificationService } from './notification.service';
import { TimeService } from './time.service';

@Injectable({
  providedIn: 'root'
})
export class FeierabendService {
  private leaveTime: DateTime;
  private timerSubscription: Subscription;

  timerFinished$ = new Subject<void>();

  constructor(
    private notification: NotificationService,
    private time: TimeService
  ) {
    this.time.output$.pipe(map(data => data.leaveTime)).subscribe(leaveTime => {
      this.leaveTime = leaveTime;
      this.deactivateTimer();
    });
    this.notification.status$
      .pipe(filter(val => val === 'denied'))
      .subscribe(() => this.deactivateTimer());
  }

  activateTimer(reminderInMinutes: number) {
    const reminderInMillis = reminderInMinutes * (60 * 1000);
    const timeTillFeierabend = this.getTimeTillFeierabend();

    let delay = timeTillFeierabend - reminderInMillis;
    if (delay < 0) {
      delay = 0;
      reminderInMinutes = Math.round(timeTillFeierabend / 60 / 1000);
    }

    this.timerSubscription = timer(delay).subscribe(() => {
      this.notification.show(
        `Feierabend`,
        this.getNotificationBody(reminderInMinutes)
      );
      this.deactivateTimer();
    });
  }

  /**
   * @returns milliseconds till feierabend
   */
  private getTimeTillFeierabend(): number {
    return this.leaveTime.diff(DateTime.local()).valueOf();
  }

  private getNotificationBody(reminderInMinutes: number) {
    return reminderInMinutes === 0
      ? `Es ist Feierabend! Time to go.`
      : reminderInMinutes > 0
        ? `${reminderInMinutes} min bis zum Feierabend.`
        : `Vor ${reminderInMinutes * -1} min war Feierabend.`;
  }

  deactivateTimer() {
    if (this.isTimerActive()) {
      this.timerSubscription.unsubscribe();
      this.timerFinished$.next();
    }
  }

  isTimerActive(): boolean {
    return !!this.timerSubscription && !this.timerSubscription.closed;
  }
}
