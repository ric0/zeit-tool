import { Injectable } from '@angular/core';
import { QuotesState } from '../modules/quotes/models/quotes-state.model';
import { DEFAULT_INPUT_VALUES } from '../shared/constants/default-input';
import { DEFAULT_QUOTE_STATE } from '../shared/constants/default-quote-state';
import { DEFAULT_SETTINGS } from '../shared/constants/default-settings';
import { DEFAULT_SURVEY_STATE } from '../shared/constants/default-survey-state';
import { convertInputToObjects, convertObjectToInput, InputModel } from '../shared/models/input.model';
import { convertObjectToSettings, convertSettingsToObject, Settings } from '../shared/models/settings.model';
import { SurveyState } from '../shared/models/survey-state.model';

interface Items {
  [item: string]: Item;
}

interface Item {
  key: string;
  default: any;
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private readonly items: Items = {
    settings: { key: 'settings', default: DEFAULT_SETTINGS },
    input: { key: 'input', default: DEFAULT_INPUT_VALUES },
    quotes: { key: 'quotes', default: DEFAULT_QUOTE_STATE },
    survey: { key: 'survey', default: DEFAULT_SURVEY_STATE }
  };

  saveSettings(s: Settings) {
    const obj = convertSettingsToObject(s);
    this.saveByKey(this.items.settings, obj);
  }
  loadSettings(): Settings {
    return this.load(this.items.settings, convertObjectToSettings);
  }
  removeSettings() {
    this.removeItemByKey(this.items.settings);
  }

  saveInput(i: InputModel) {
    const obj = convertInputToObjects(i);
    this.saveByKey(this.items.input, obj);
  }
  loadInput(): InputModel {
    return this.load(this.items.input, convertObjectToInput);
  }
  removeInput() {
    this.removeItemByKey(this.items.input);
  }

  saveQuotesState(q: QuotesState) {
    this.saveByKey(this.items.quotes, q);
  }
  loadQuotesState(): QuotesState {
    return this.load(this.items.quotes);
  }
  removeQuotesState() {
    this.removeItemByKey(this.items.quotes);
  }

  saveSurveyState(s: SurveyState) {
    this.saveByKey(this.items.survey, s);
  }
  loadSurveyState(): SurveyState {
    return this.load(this.items.survey);
  }
  removeSurveyState() {
    this.removeItemByKey(this.items.survey);
  }

  clear() {
    localStorage.clear();
  }

  private saveByKey({ key }: { key: string }, obj: any) {
    localStorage.setItem(key, JSON.stringify(obj));
  }

  private load(item: Item, convertFn?: (obj: any) => any) {
    try {
      const obj = this.loadByKey(item);
      return convertFn ? convertFn(obj) : obj;
    } catch (e) {
      return item.default;
    }
  }

  private loadByKey({ key }: { key: string }): any {
    const obj: string = localStorage.getItem(key);
    if (!obj) {
      throw new Error(`No value stored in Localstorage for key: ${key}`);
    }
    return JSON.parse(obj);
  }

  private removeItemByKey({ key }: { key: string }) {
    localStorage.removeItem(key);
  }
}
