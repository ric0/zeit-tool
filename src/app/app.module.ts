import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FootersModule } from './modules/footers/footers.module';
import { HeadersModule } from './modules/headers/headers.module';
import { SurveyModule } from './modules/survey/survey.module';
import { TimeModule } from './modules/time/time.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UpdateTimerService } from './services/update-timer.service';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { noop } from './shared/util';

/**
 * If I understand the documentation correctly, the build flag --i18nLocalId
 * will automatically choose and apply the correct Language
 *
 * EN is registered by default
 *
 * See: https://angular.io/guide/i18n#i18n-pipes
 */
registerLocaleData(localeDe, 'de');
registerLocaleData(localeEn, 'en');

/**
 * ngx-translate/http-loader
 */
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    HeadersModule,
    FootersModule,
    TimeModule,
    SurveyModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: noop,
      deps: [UpdateTimerService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
