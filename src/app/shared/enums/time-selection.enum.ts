export enum TimeSelection {
  DefaultWorkingTime = 'defaultWorkingTime',
  WorkingTime = 'workingTime',
  LeaveTime = 'leaveTime'
}
