export enum Balance {
    Positive = 'positive',
    Neutral = 'neutral',
    Negative = 'negative'
}
