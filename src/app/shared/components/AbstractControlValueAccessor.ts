import { Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlValueAccessor, ValidationErrors, Validator } from '@angular/forms';
import { Subject } from 'rxjs';

const noop = (obj?: any) => {
};

export abstract class AbstractControlValueAccessor
  implements OnInit, OnDestroy, ControlValueAccessor, Validator {
  @Input() label = '';
  protected destroy$ = new Subject<void>();
  protected emitChangedFn = noop;
  protected emitTouchedFn = noop;
  protected emitValidatorsChangedFn = noop;
  protected onInitFn = noop;

  abstract writeValue(obj: any): void;

  abstract validate(control: AbstractControl): ValidationErrors;

  setDisabledState?(isDisabled: boolean): void {
  }


  ngOnInit(): void {
    this.onInitFn();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  registerOnChange(fn: any): void {
    this.emitChangedFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.emitTouchedFn = fn;
  }

  registerOnValidatorChange?(fn: () => void): void {
    this.emitValidatorsChangedFn = fn;
  }
}
