import { Component, forwardRef, Input } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validators } from '@angular/forms';
import { DateTime, Duration } from 'luxon';
import { debounceTime, distinctUntilChanged, filter, map, takeUntil, tap } from 'rxjs/operators';
import { convertStringToDateTime, convertStringToDuration } from '../../helper/luxon-conversion-helper';
import { TIME_REGEX } from '../../regex/input-regex';
import { AbstractControlValueAccessor } from '../AbstractControlValueAccessor';
import { TimeConstants } from '../../constants/time-constants';

type TimeType = 'Duration' | 'DateTime';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => TimeInputComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => TimeInputComponent),
  multi: true
};

@Component({
  selector: 'app-time-input',
  templateUrl: './time-input.component.html',
  styleUrls: ['./time-input.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class TimeInputComponent extends AbstractControlValueAccessor {
  @Input() timeType: TimeType;
  @Input() appendUhrLabel = false;
  control: FormControl;
  validValue = '';

  constructor() {
    super();
  }

  onInitFn = () => {
    this.control = new FormControl();
    this.setValidators();
    this.listenToValueChanges();
  };

  writeValue(obj: DateTime | Duration): void {
    this.setValue(obj, { emitEvent: false });
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.control.errors;
  }

  onFocus(inputRef) {
    inputRef.select();
  }

  onFocusOut() {
    const value = this.control.valid ? this.control.value : this.validValue;
    const formattedValue = this.formatValue(value);
    this.control.setValue(formattedValue, { emitEvent: false });
    this.emitTouchedFn();
  }

  /**
   * Listen to keyDown-Events
   *
   * https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
   * Internet Explorer, Edge (16 and earlier), and Firefox (36 and earlier) use "Left", "Right", "Up", and "Down"
   * instead of "ArrowLeft", "ArrowRight", "ArrowUp", and "ArrowDown".
   *
   * @param event to identify the pressed button
   */
  onKeyDown(event: KeyboardEvent) {
    if (this.control.invalid) {
      return;
    }

    switch (event.key) {
      case 'ArrowUp':
        this.incrementValue();
        break;
      case 'ArrowDown':
        this.decrementValue();
        break;
    }
  }

  private setValidators() {
    this.control.setValidators([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(5),
      Validators.pattern(TIME_REGEX)
    ]);
    this.emitValidatorsChangedFn();
  }

  private listenToValueChanges() {
    this.control.valueChanges
      .pipe(
        map(val => this.filterInput(val)),
        debounceTime(300),
        distinctUntilChanged(),
        filter(() => this.control.valid),
        tap(val => (this.validValue = val)),
        map(val => this.convertToObject(val)),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        this.emitChangedFn(value);
      });
  }

  private filterInput(value: string): string {
    const filtered = value.replace(/[^\d:]/g, '');
    this.control.setValue(filtered, { emitEvent: false });
    return filtered;
  }

  /**
   * Format:
   *    HH:mm
   * or
   *    H:mm
   * @param input by the User
   */
  private formatValue(input: string): string {
    let output: string;

    if (input.includes(':')) {
      output = input;
    } else if (input.length === 3) {
      output = input[0] + ':' + input[1] + input[2];
    } else {
      output = input[0] + input[1] + ':' + input[2] + input[3];
    }

    return output;
  }

  private incrementValue() {
    const current = this.convertToObject(this.control.value);
    const inc = current.plus(TimeConstants.ONE_MINUTE);
    this.setValue(inc);
  }

  private decrementValue() {
    const current = this.convertToObject(this.control.value);
    const dec = current.minus(TimeConstants.ONE_MINUTE);
    this.setValue(dec);
  }

  private convertToObject(val: string): Duration | DateTime {
    return this.timeType === 'DateTime'
      ? convertStringToDateTime(val)
      : convertStringToDuration(val);
  }

  private setValue(obj: DateTime | Duration, options: { emitEvent: boolean } = { emitEvent: true }) {
    const value = this.convertToString(obj);
    this.validValue = value;
    this.control.setValue(value, options);
  }

  private convertToString(obj: DateTime | Duration) {
    let value = '';
    if (DateTime.isDateTime(obj)) {
      this.timeType = 'DateTime';
      value = obj.toFormat('H:mm');
    } else if (Duration.isDuration(obj)) {
      this.timeType = 'Duration';
      value = obj.toFormat('h:mm');
    } else {
      throw new Error(
        `Unable to convert to string, obj: ${JSON.stringify(obj)}`
      );
    }
    return value;
  }
}
