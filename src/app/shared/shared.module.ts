import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TimeInputComponent } from './components/time-input/time-input.component';
import { InputFormatterDirective } from './directives/input-formatter.directive';
import { MaterialImportsModule } from './material-imports/material-imports.module';
import { LuxonFormatPipe } from './pipes/luxon-format.pipe';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [InputFormatterDirective, LuxonFormatPipe, TimeInputComponent],
  imports: [CommonModule, ReactiveFormsModule, MaterialImportsModule],
  exports: [
    ReactiveFormsModule,
    MaterialImportsModule,
    TranslateModule,
    InputFormatterDirective,
    LuxonFormatPipe,
    TimeInputComponent
  ]
})
export class SharedModule {
}
