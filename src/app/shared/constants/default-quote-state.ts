import { QuotesState } from 'src/app/modules/quotes/models/quotes-state.model';

export const DEFAULT_QUOTE_STATE: QuotesState = {
  disliked: [],
  viewed: []
};
