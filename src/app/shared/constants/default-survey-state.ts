import { SurveyState } from '../models/survey-state.model';

export const DEFAULT_SURVEY_STATE: SurveyState = {
  entries: []
};
