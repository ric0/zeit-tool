import { InputModel } from '../models/input.model';
import { TimeConstants } from './time-constants';
import { TimeSelection } from '../enums/time-selection.enum';

export const DEFAULT_INPUT_VALUES: InputModel = {
  arrivalTime: TimeConstants.DEFAULT_ARRIVAL_TIME,
  timeSelection: TimeSelection.DefaultWorkingTime,
  workingTime: TimeConstants.DEFAULT_WORKING_TIME,
  leaveTime: TimeConstants.DEFAULT_LEAVING_TIME,
  break: {
    selected: false,
    breaks: []
  }
};
