import { DateTime, Duration } from 'luxon';

export class TimeConstants {

  static readonly ZERO_SECONDS: Duration = Duration.fromMillis(0);
  static readonly ONE_MINUTE: Duration = Duration.fromObject({ minutes: 1 });
  static readonly FIFTEEN_MINUTES: Duration = Duration.fromObject({ minutes: 15 });
  static readonly THIRTY_MINUTES: Duration = Duration.fromObject({ minutes: 30 });
  static readonly FORTY_FIVE_MINUTES: Duration = Duration.fromObject({ minutes: 45 });

  static readonly ONE_HOUR: Duration = Duration.fromObject({ hours: 1 });
  static readonly THREE_HOURS: Duration = Duration.fromObject({ hours: 3 });
  static readonly SIX_HOURS: Duration = Duration.fromObject({ hours: 6 });
  static readonly NINE_HOURS: Duration = Duration.fromObject({ hours: 9 });
  static readonly TEN_HOURS: Duration = Duration.fromObject({ hours: 10 });
  static readonly TEN_HOURS_FORTYFIVE_MINS: Duration = Duration.fromObject({ hours: 10, minutes: 45 });
  static readonly ONE_DAY: Duration = Duration.fromObject({ day: 1 });

  static readonly DEFAULT_ARRIVAL_TIME: DateTime = DateTime.fromFormat('06:00', 'H:mm');
  static readonly DEFAULT_BREAK_TIME: Duration = TimeConstants.THIRTY_MINUTES;
  static readonly DEFAULT_WORKING_TIME: Duration = Duration.fromObject({ hours: 7, minutes: 48 });
  static readonly DEFAULT_LEAVING_TIME: DateTime = DateTime.fromFormat('15:00', 'H:mm');
}
