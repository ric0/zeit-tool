import { Category } from 'src/app/modules/quotes/models/category.enum';
import { Settings } from '../models/settings.model';
import { TimeConstants } from './time-constants';

export const DEFAULT_SETTINGS: Settings = {
  defaultWorkingTime: TimeConstants.DEFAULT_WORKING_TIME,
  enableAutomaticArrivalTime: false,
  enableTabCloseWarning: false,
  quotes: {
    showQuotes: true,
    selectedCategories: [Category.POSITIVE]
  }
};
