import { Pipe, PipeTransform } from '@angular/core';
import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../constants/time-constants';

@Pipe({ name: 'luxonFormat' })
export class LuxonFormatPipe implements PipeTransform {
  transform(value: DateTime | Duration, format: string, formatGreaterOneHour?: string): string {
    if (!value) {
      return '';
    }

    try {
      if (Duration.isDuration(value)) {
        value = value.normalize();
      }
      return formatGreaterOneHour && this.isMoreThanOneHour(value as Duration)
        ? value.toFormat(formatGreaterOneHour)
        : value.toFormat(format);
    } catch (e) {
      throw new Error('Value is neither a DateTime or Duration OR invalid');
    }
  }

  private isMoreThanOneHour(value: Duration) {
    return value > TimeConstants.ONE_HOUR;
  }
}
