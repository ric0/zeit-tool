import { Duration } from 'luxon';
import { Balance } from '../enums/balance.enum';

export interface TimeDiff {
    balance: Balance;
    value: Duration;
}
