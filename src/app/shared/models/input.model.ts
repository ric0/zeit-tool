import { DateTime, Duration } from 'luxon';
import { TimeSelection } from '../enums/time-selection.enum';
import { BreakInputModel, convertBreakInputToObject, convertObjectToBreakInputModel } from './break-input.model';
import { toObjectOrNull } from '../util';

export interface InputModel {
  arrivalTime: DateTime;
  timeSelection: TimeSelection;
  workingTime: Duration;
  leaveTime: DateTime;
  break: BreakInputModel;
}

// ----------------------------------- \\
//                                     \\
//           HELPER FUNCTIONS          \\
//                                     \\
// ----------------------------------- \\

export function convertInputToObjects(i: InputModel): any {
  return {
    arrivalTime: toObjectOrNull(i.arrivalTime),
    timeSelection: i.timeSelection,
    workingTime: toObjectOrNull(i.workingTime),
    leaveTime: toObjectOrNull(i.leaveTime),
    break: convertBreakInputToObject(i.break)
  };
}

export function convertObjectToInput(obj: any): InputModel {
  const today = DateTime.local();

  const arrivalTime = DateTime.fromObject(obj.arrivalTime).set({
    year: today.year,
    month: today.month,
    day: today.day
  });

  const leaveTime = DateTime.fromObject(obj.leaveTime).set({
    year: today.year,
    month: today.month,
    day: today.day
  });

  return {
    arrivalTime,
    timeSelection: obj.timeSelection,
    workingTime: Duration.fromObject(obj.workingTime),
    leaveTime,
    break: convertObjectToBreakInputModel(obj.break)
  };
}

export function resetDayMonthYear(input: InputModel): InputModel {
  const { day, month, year } = DateTime.local();
  return {
    ...input,
    arrivalTime: input.arrivalTime.set({ day, month, year }),
    leaveTime: input.leaveTime.set({ day, month, year })
  };
}
