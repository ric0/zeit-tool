export interface SurveyStateEntry {
  id: number;
  status: SurveyStatusType;
}

export type SurveyStatusType = 'accepted' | 'declined' | 'later';

export interface SurveyState {
  entries: SurveyStateEntry[];
}
