import { Duration } from 'luxon';
import { Category } from 'src/app/modules/quotes/models/category.enum';

export interface Settings {
  defaultWorkingTime: Duration;
  enableAutomaticArrivalTime: boolean;
  enableTabCloseWarning: boolean;
  quotes: {
    showQuotes: boolean;
    selectedCategories: Category[];
  };
}

// ----------------------------------- \\
//                                     \\
//           HELPER FUNCTIONS          \\
//                                     \\
// ----------------------------------- \\
/*
 * Some values are Luxons-DateTime and -Duration objects.
 * In order to access their functions, these have to be
 * and converted back to JS-Objects, and saved as simple
 * JSON-objects.
 */

export function convertSettingsToObject(s: Settings): any {
  return {
    ...s,
    defaultWorkingTime: s.defaultWorkingTime.toObject()
  };
}

export function convertObjectToSettings(obj: any): Settings {
  return {
    ...obj,
    defaultWorkingTime: Duration.fromObject(obj.defaultWorkingTime)
  };
}
