import { Duration } from 'luxon';

export interface BreakTime {
  required: Duration;
  additional: Duration;
  total: Duration;
}
