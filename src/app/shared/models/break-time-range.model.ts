import { DateTime } from 'luxon';

export interface BreakRange {
  begin: DateTime;
  end: DateTime;
}

// ----------------------------------- \\
//                                     \\
//           HELPER FUNCTIONS          \\
//                                     \\
// ----------------------------------- \\

export function convertBreakRangeToObject(r: BreakRange): any {
  return {
    begin: r.begin.toObject(),
    end: r.end.toObject()
  };
}

export function convertObjectDataToBreakRange(obj: any): BreakRange {
  if (!obj.begin || !obj.end) {
    throw new Error('Object does not qualify as a BreakRange');
  }
  return {
    begin: DateTime.fromObject(obj.begin),
    end: DateTime.fromObject(obj.end)
  };
}
