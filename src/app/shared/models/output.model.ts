import { DateTime, Duration } from 'luxon';
import { BreakTime } from './break-time';
import { TimeDiff } from './time-diff';

export interface OutputModel {
    arrivalTime: DateTime;
    leaveTime: DateTime;
    workingTime: Duration;
    breakTime: BreakTime;
    overtime: TimeDiff;
}
