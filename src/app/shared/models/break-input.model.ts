import { BreakRange, convertBreakRangeToObject, convertObjectDataToBreakRange } from './break-time-range.model';
import { Duration, DurationObject } from 'luxon';
import { TimeConstants } from '../constants/time-constants';

export enum BreakInputType {
  RANGE = 'range',
  DURATION = 'duration'
}

export interface BreakInputData {
  type: BreakInputType;
  data: BreakRange | Duration;
}

export interface BreakInputModel {
  selected: boolean;
  breaks?: BreakInputData[];
}

// ----------------------------------- \\
//                                     \\
//           HELPER FUNCIONS           \\
//                                     \\
// ----------------------------------- \\

enum Filter {
  ELIGIBLE,
  INELIGIBLE,
  NONE
}

/**
 * https://www.gesetze-im-internet.de/arbzg/__4.html
 * only breaks longer than 15min are permitted by law
 */
export function extractEligibleBreakSum(b: BreakInputModel): Duration {
  return extractBreakSum(b, Filter.ELIGIBLE);
}

export function extractIneligibleBreakSum(b: BreakInputModel): Duration {
  return extractBreakSum(b, Filter.INELIGIBLE);
}

function extractBreakSum(b: BreakInputModel, filter: Filter = Filter.NONE): Duration {
  if (!b.breaks) {
    return TimeConstants.ZERO_SECONDS;
  }
  const breaks: Duration[] = b.breaks.map(extractValue);
  let filteredBreaks: Duration[] = breaks;

  if (filter === Filter.ELIGIBLE) {
    filteredBreaks = breaks.filter(e => e.valueOf() >= TimeConstants.FIFTEEN_MINUTES.valueOf());

  } else if (filter === Filter.INELIGIBLE) {
    filteredBreaks = breaks.filter(e => e.valueOf() < TimeConstants.FIFTEEN_MINUTES.valueOf());
  }

  return filteredBreaks.reduce(
    (acc, curr) => acc.plus(curr),
    TimeConstants.ZERO_SECONDS
  );
}

function extractValue(b: BreakInputData): Duration {
  let val: Duration;

  if (b.type === BreakInputType.RANGE) {
    const { begin, end } = b.data as BreakRange;
    val = end.diff(begin);
  } else {
    val = b.data as Duration;
  }

  return val;
}

export function convertBreakInputToObject(b: BreakInputModel): any {
  return {
    ...b,
    breaks: [...b.breaks.map(d => ({ ...d, data: convertDataToObject(d) }))]
  };
}

function convertDataToObject(d: BreakInputData): any {
  if (d.type === BreakInputType.RANGE) {
    return convertBreakRangeToObject(d.data as BreakRange);
  } else {
    return (d.data as Duration).toObject();
  }
}

export function convertObjectToBreakInputModel(obj: any): BreakInputModel {
  return {
    ...obj,
    breaks: [
      ...obj.breaks.map(e => ({ type: e.type, data: convertObjectToData(e) }))
    ]
  };
}

function convertObjectToData({ type, data }: BreakInputData): BreakRange | Duration {
  if (type === BreakInputType.RANGE) {
    return convertObjectDataToBreakRange(data);
  } else {
    return Duration.fromObject(data as DurationObject);
  }
}
