import { DateTime, Duration } from 'luxon';
import { TimeConstants } from '../constants/time-constants';
import { TIME_REGEX } from '../regex/input-regex';

export function convertStringToDateTime(str: string): DateTime {
  if (!str.match(TIME_REGEX)) {
    throw new Error(`Wrong Format: ${str}, expected: H:mm`);
  }

  const res: DateTime = str.includes(':')
    ? DateTime.fromFormat(str, 'H:mm')
    : str.length === 3
      ? DateTime.fromFormat(str, 'Hmm')
      : DateTime.fromFormat(str, 'HHmm');
  return res.set({ second: 0, millisecond: 0 });
}

export function convertStringToDuration(str: string): Duration {
  if (!str) {
    return TimeConstants.ZERO_SECONDS;
  } else if (!str.match(TIME_REGEX)) {
    throw new Error(`Wrong Format: ${str}, expected: H:mm`);
  }

  const [hours, minutes] = str.includes(':')
    ? str.split(':').map(val => +val)
    : str.length === 3
      ? [str[0], str[1] + str[2]].map(val => +val)
      : [str[0] + str[1], str[2] + str[3]].map(val => +val);
  const seconds = 0;
  const milliseconds = 0;

  return Duration.fromObject({ hours, minutes, seconds, milliseconds });
}
