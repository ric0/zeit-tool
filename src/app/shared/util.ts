import { DateTime, Duration } from 'luxon';

export function convertArrayToObject(arr: any[], eachValue: any = null): any {
  return arr.reduce((acc, curr) => {
    acc[curr] = eachValue;
    return acc;
  }, {});
}

export function toObjectOrNull(val: Duration | DateTime): any | null {
  return val ? val.toObject() : null;
}

// Noop handler for factory function
export function noop() {
  return () => {
  };
}​
