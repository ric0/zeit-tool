/**
 * possible input formats for Time:
 *
 *  H:mm    Hmm
 *  HH:mm  HHmm
 *
 *  0:00 - 23:59
 */
export const TIME_REGEX: RegExp = /^(2[0-3]|[0-1][0-9]|[0-9]):?[0-5][0-9]$/;
