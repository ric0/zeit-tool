import { TIME_REGEX } from './input-regex';

describe('Testing the Time-Regex', () => {
  it('positive tests', () => {

    expect('000'.match(TIME_REGEX)).toBeTruthy();
    expect('0:00'.match(TIME_REGEX)).toBeTruthy();
    expect('0000'.match(TIME_REGEX)).toBeTruthy();
    expect('00:00'.match(TIME_REGEX)).toBeTruthy();

    expect('1200'.match(TIME_REGEX)).toBeTruthy();
    expect('12:00'.match(TIME_REGEX)).toBeTruthy();

    expect('2359'.match(TIME_REGEX)).toBeTruthy();
    expect('23:59'.match(TIME_REGEX)).toBeTruthy();
  });

  it('negative tests', () => {
    expect(''.match(TIME_REGEX)).toBeFalsy();
    expect('a'.match(TIME_REGEX)).toBeFalsy();
    expect('aa'.match(TIME_REGEX)).toBeFalsy();
    expect('aaa'.match(TIME_REGEX)).toBeFalsy();
    expect('aaaa'.match(TIME_REGEX)).toBeFalsy();
    expect('a:aa'.match(TIME_REGEX)).toBeFalsy();
    expect('aa:aa'.match(TIME_REGEX)).toBeFalsy();

    expect(':'.match(TIME_REGEX)).toBeFalsy();
    expect(':12'.match(TIME_REGEX)).toBeFalsy();
    expect('12:'.match(TIME_REGEX)).toBeFalsy();

    expect('99'.match(TIME_REGEX)).toBeFalsy();
    expect('999'.match(TIME_REGEX)).toBeFalsy();
    expect('99999'.match(TIME_REGEX)).toBeFalsy();

    expect('111:12'.match(TIME_REGEX)).toBeFalsy();
    expect('11112'.match(TIME_REGEX)).toBeFalsy();

    expect('1:2'.match(TIME_REGEX)).toBeFalsy();
    expect('11:73'.match(TIME_REGEX)).toBeFalsy();
    expect('31:73'.match(TIME_REGEX)).toBeFalsy();
    expect('23:60'.match(TIME_REGEX)).toBeFalsy();
    expect('25:00'.match(TIME_REGEX)).toBeFalsy();
    expect('2500'.match(TIME_REGEX)).toBeFalsy();
    expect('25:59'.match(TIME_REGEX)).toBeFalsy();
    expect('2559'.match(TIME_REGEX)).toBeFalsy();
    expect('-12:00'.match(TIME_REGEX)).toBeFalsy();
    expect('-2:33'.match(TIME_REGEX)).toBeFalsy();

    expect('3000'.match(TIME_REGEX)).toBeFalsy();
    expect('30:00'.match(TIME_REGEX)).toBeFalsy();

    expect('24:01'.match(TIME_REGEX)).toBeFalsy();
    expect('2401'.match(TIME_REGEX)).toBeFalsy();

    expect('01:01a'.match(TIME_REGEX)).toBeFalsy();
    expect('0l:01'.match(TIME_REGEX)).toBeFalsy();
  });
});
