import { Directive, ElementRef, HostListener } from '@angular/core';
import { TIME_REGEX } from '../regex/input-regex';

@Directive({
  selector: '[appInputFormatter]'
})
export class InputFormatterDirective {

  constructor(private el: ElementRef) {
  }

  /**
   * Upon focus lost (User selects another element, OR clicks outside the input)
   * The Input will be automatically formatted to HH:mm
   */
  @HostListener('blur') onFocusLost() {
    const val: string = this.el.nativeElement.value;

    if (val.match(TIME_REGEX) && this.el.nativeElement.type === 'text') {
      this.el.nativeElement.value = this.formatValue(val);
    }
  }

  /**
   * Format:
   *    HH:mm
   * or
   *    H:mm
   * @param input by the User
   */
  private formatValue(input: string): string {
    let output: string;

    if (input.includes(':')) {
      output = input;
    } else if (input.length === 3) {
      output = input[0] + ':' + input[1] + input[2];
    } else {
      output = input[0] + input[1] + ':' + input[2] + input[3];
    }

    return output;
  }
}
