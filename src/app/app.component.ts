import { Component, HostListener, Inject, LOCALE_ID } from '@angular/core';
import { SettingsService } from './services/settings.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @HostListener('window:beforeunload', ['$event'])
  private onbeforeunload = null;

  constructor(@Inject(LOCALE_ID) public locale: string,
              private translate: TranslateService,
              private settings: SettingsService
  ) {
    this.translate.setDefaultLang('en');
    this.translate.use(locale);

    this.onbeforeunload = this.handleOnbeforeunload;
  }

  /**
   * https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload
   */
  private handleOnbeforeunload = ($event) => {
    if (this.settings.getValues().enableTabCloseWarning) {
      $event.preventDefault();
      $event.returnValue = ''; // Chrome requires returnValue to be set

    } else {
      delete $event.returnValue;
    }
  };
}
