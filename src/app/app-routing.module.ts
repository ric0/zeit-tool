import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimeComponent } from './modules/time/components/time/time.component';

const routes: Routes = [
  { path: '', component: TimeComponent, pathMatch: 'full' },
  {
    path: 'settings',
    loadChildren: () =>
      import('./modules/settings/settings.module').then(m => m.SettingsModule)
  }, {
    path: 'feedback',
    loadChildren: () =>
      import('./modules/feedback/feedback.module').then(m => m.FeedbackModule)
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
