import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeierabendComponent } from './feierabend.component';

describe('FeierabendComponent', () => {
  let component: FeierabendComponent;
  let fixture: ComponentFixture<FeierabendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeierabendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeierabendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
