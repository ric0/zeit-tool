import { Platform } from '@angular/cdk/platform';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feierabend',
  templateUrl: './feierabend.component.html',
  styleUrls: ['./feierabend.component.scss']
})
export class FeierabendComponent implements OnInit {
  constructor(public platform: Platform) {}

  ngOnInit() {}
}
