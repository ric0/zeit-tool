import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeierabendInputComponent } from './feierabend-input.component';

describe('InputComponent', () => {
  let component: FeierabendInputComponent;
  let fixture: ComponentFixture<FeierabendInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeierabendInputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeierabendInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
