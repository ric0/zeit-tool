import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DateTime } from 'luxon';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { FeierabendService } from 'src/app/services/feierabend.service';
import { NotificationService } from 'src/app/services/notification.service';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: 'app-feierabend-input',
  templateUrl: './feierabend-input.component.html',
  styleUrls: ['./feierabend-input.component.scss']
})
export class FeierabendInputComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  form: FormGroup;

  leaveTime$: Observable<DateTime>;
  areNotificationPermissionsGranted = false;

  constructor(
    private fb: FormBuilder,
    private time: TimeService,
    private notification: NotificationService,
    private feierabend: FeierabendService
  ) {}

  ngOnInit() {
    this.setupForm();
    this.subscribeToNotificationPermissionStatus();
    this.subscribeToBerechnungsUpdates();
    this.subscribeToTimerFinished();
  }

  ngOnDestroy() {
    this.feierabend.deactivateTimer();
    this.destroy$.next();
    this.destroy$.complete();
  }

  private setupForm() {
    this.form = this.fb.group({
      reminderInMinutes: [30, [Validators.required, Validators.min(0)]],
      slider: [false, [Validators.required]]
    });
  }

  private subscribeToNotificationPermissionStatus() {
    this.notification.status$
      .pipe(takeUntil(this.destroy$))
      .subscribe(status => {
        const slider = this.form.controls.slider;
        status === 'denied' ? slider.disable() : slider.enable();
        this.areNotificationPermissionsGranted = status === 'granted';
      });
  }

  private subscribeToBerechnungsUpdates() {
    this.leaveTime$ = this.time.output$.pipe(
      map(output => output.leaveTime),
      takeUntil(this.destroy$)
    );
  }

  private subscribeToTimerFinished() {
    this.feierabend.timerFinished$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.form.controls.slider.setValue(false);
      });
  }

  onToggleFeierabendTimer(slider: MatSlideToggleChange) {
    this.feierabend.deactivateTimer();
    this.notification.requestPermission();

    if (slider.checked && this.form.valid) {
      this.feierabend.activateTimer(this.form.value.reminderInMinutes);
    }
  }
}
