import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FeierabendInputComponent } from './feierabend-input/feierabend-input.component';
import { FeierabendComponent } from './feierabend/feierabend.component';

@NgModule({
  declarations: [FeierabendComponent, FeierabendInputComponent],
  imports: [CommonModule, RouterModule, SharedModule],
  exports: [FeierabendComponent]
})
export class FeierabendModule {}
