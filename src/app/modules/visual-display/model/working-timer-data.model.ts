import { Duration } from 'luxon';
import { TimeDiff } from 'src/app/shared/models/time-diff';

export interface WorkingTimerData {
    currentWorkingTime: Duration;
    percentageOfMaxAttendanceTime: number;
    percentageOfMaxAttendanceTimeInverted: number;
    balance: TimeDiff;
}
