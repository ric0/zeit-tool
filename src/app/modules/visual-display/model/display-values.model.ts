export interface DisplayValues {
  arrivalTime: string;
  sixHours: string;
  nineHours: string;
  tenHours: string;

  contractedLeaveTime: {
    text: string;
    percentage: number;
  };
}
