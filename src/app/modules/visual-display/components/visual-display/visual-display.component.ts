import { Component, OnInit } from '@angular/core';
import { Duration } from 'luxon';
import { BehaviorSubject } from 'rxjs';
import { DisplayValues } from '../../model/display-values.model';
import { DisplayValuesService } from '../../services/display-values.service';
import { WorkingTimerData } from '../../model/working-timer-data.model';
import { Balance } from 'src/app/shared/enums/balance.enum';

@Component({
  selector: 'app-visual-display',
  templateUrl: './visual-display.component.html',
  styleUrls: ['./visual-display.component.scss']
})
export class VisualDisplayComponent implements OnInit {
  Balance = Balance;

  displayValues: DisplayValues;
  progressBarWidth = 100;
  currentWorkTime: Duration;
  contractedLeaveTimePercentageFromLeft = 74.41;

  timer$: BehaviorSubject<WorkingTimerData>;
  displayValues$: BehaviorSubject<DisplayValues>;

  constructor(private service: DisplayValuesService) {}

  ngOnInit() {
    this.displayValues$ = this.service.displayValues$;
    this.timer$ = this.service.attendanceTimeTicker$;
  }
}
