import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { VisualDisplayComponent } from './components/visual-display/visual-display.component';

@NgModule({
  declarations: [VisualDisplayComponent],
  imports: [CommonModule, SharedModule],
  exports: [VisualDisplayComponent]
})
export class VisualDisplayModule {}
