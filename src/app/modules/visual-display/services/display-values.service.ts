import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { BehaviorSubject, Subscription, timer } from 'rxjs';
import { CalculationService } from 'src/app/services/calculation.service';
import { TimeService } from 'src/app/services/time.service';
import { TimeConstants } from 'src/app/shared/constants/time-constants';
import { Balance } from 'src/app/shared/enums/balance.enum';
import { TimeSelection } from 'src/app/shared/enums/time-selection.enum';
import { InputModel } from 'src/app/shared/models/input.model';
import { OutputModel } from 'src/app/shared/models/output.model';
import { TimeDiff } from 'src/app/shared/models/time-diff';
import { DisplayValues } from '../model/display-values.model';
import { WorkingTimerData } from '../model/working-timer-data.model';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DisplayValuesService {
  displayValues$: BehaviorSubject<DisplayValues>;
  attendanceTimeTicker$: BehaviorSubject<WorkingTimerData>;

  private timerSub: Subscription;

  constructor(private time: TimeService, private calc: CalculationService) {
    this.setupInitialValues();

    this.time.output$.subscribe((output: OutputModel) => {
      this.publishDisplayValues(output);
      this.setupTimer(output.arrivalTime);
    });
  }

  private setupInitialValues() {
    this.time.output$
      .pipe(first()) // takes the initially calculated values
      .subscribe(initialOutput => {
        const initialDisplayValues = this.calculateDisplayValues(initialOutput);
        this.displayValues$ = new BehaviorSubject<DisplayValues>(initialDisplayValues);

        const initialWorkingTimerData = this.calcWorkingTimerData(initialOutput.arrivalTime);
        this.attendanceTimeTicker$ = new BehaviorSubject<WorkingTimerData>(initialWorkingTimerData);
      });
  }

  private publishDisplayValues(output: OutputModel) {
    const displayValues: DisplayValues = this.calculateDisplayValues(output);
    this.displayValues$.next(displayValues);
  }

  private calculateDisplayValues(output: OutputModel): DisplayValues {
    const arrival = output.arrivalTime;
    const defaultLeaveTime = this.calcDefaultLeaveTime(output);
    const percentage =
      defaultLeaveTime.diff(arrival).valueOf() /
      TimeConstants.TEN_HOURS_FORTYFIVE_MINS.valueOf();

    return {
      arrivalTime: arrival.toFormat('H:mm'),
      sixHours: arrival.plus(TimeConstants.SIX_HOURS).toFormat('H:mm'),
      nineHours: arrival
        .plus(TimeConstants.NINE_HOURS)
        .plus(TimeConstants.THIRTY_MINUTES)
        .toFormat('H:mm'),
      tenHours: arrival
        .plus(TimeConstants.TEN_HOURS)
        .plus(TimeConstants.FORTY_FIVE_MINUTES)
        .toFormat('H:mm'),
      contractedLeaveTime: {
        text: defaultLeaveTime.toFormat('H:mm'),
        percentage: percentage * 100
      }
    };
  }

  private calcDefaultLeaveTime(output: OutputModel): DateTime {
    const calculatedOutput = this.calc.calculate({
      arrivalTime: output.arrivalTime,
      timeSelection: TimeSelection.DefaultWorkingTime,
      workingTime: null,
      leaveTime: null,
      break: {
        selected: false
      }
    });

    return calculatedOutput.leaveTime;
  }

  private setupTimer(arrivalTime: DateTime) {
    if (this.timerSub && !this.timerSub.closed) {
      this.timerSub.unsubscribe();
    }
    this.publishAttendanceTimerData(arrivalTime);
    this.initAttendanceTimer(arrivalTime);
  }

  private initAttendanceTimer(arrivalTime: DateTime) {
    const secondsTillFullMinute = 60 - DateTime.local().second;
    const delay = secondsTillFullMinute * 1000;
    const everyMinute = 60 * 1000;

    this.timerSub = timer(delay, everyMinute).subscribe(() => {
      this.publishAttendanceTimerData(arrivalTime);
    });
  }

  private publishAttendanceTimerData(arrivalTime: DateTime) {
    const attendanceTimerData = this.calcWorkingTimerData(arrivalTime);
    this.attendanceTimeTicker$.next(attendanceTimerData);
  }

  private calcWorkingTimerData(arrivalTime: DateTime): WorkingTimerData {
    const input: InputModel = {
      arrivalTime,
      timeSelection: TimeSelection.LeaveTime,
      leaveTime: DateTime.local(),
      workingTime: null,
      break: {
        selected: false
      }
    };
    const output = this.calc.calculate(input);
    const attendanceTime = output.workingTime.plus(output.breakTime.total);
    const timeInMinutes = attendanceTime.valueOf() / 1000 / 60;
    const maxAttendanceTime = 10 * 60 + 45; // 10h Worktime + 45min Breaktime
    const percentage = (timeInMinutes / maxAttendanceTime) * 100;
    const percentageInverted = 100.0 - percentage;

    const balance: TimeDiff = {
      ...output.overtime,
      value:
        output.overtime.balance === Balance.Negative
          ? output.overtime.value.plus({ minutes: 1 })
          : output.overtime.value
    };

    return {
      currentWorkingTime: output.workingTime,
      percentageOfMaxAttendanceTime: percentage,
      percentageOfMaxAttendanceTimeInverted: percentageInverted,
      balance
    };
  }
}
