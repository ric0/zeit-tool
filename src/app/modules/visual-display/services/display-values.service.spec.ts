import { TestBed } from '@angular/core/testing';

import { DisplayValuesService } from './display-values.service';

describe('DisplayValuesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisplayValuesService = TestBed.inject(DisplayValuesService);
    expect(service).toBeTruthy();
  });
});
