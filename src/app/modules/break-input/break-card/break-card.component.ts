import { Component, forwardRef } from '@angular/core';
import { AbstractControlValueAccessor } from 'src/app/shared/components/AbstractControlValueAccessor';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validators } from '@angular/forms';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { BreakInputModel } from '../../../shared/models/break-input.model';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakCardComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakCardComponent),
  multi: true
};

@Component({
  selector: 'app-break-card',
  templateUrl: './break-card.component.html',
  styleUrls: ['./break-card.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class BreakCardComponent extends AbstractControlValueAccessor {
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    super();
  }

  onInitFn = () => {
    this.form = this.fb.group({
      selected: [null, [Validators.required]],
      breaks: [null]
    });

    this.form.valueChanges
      .pipe(
        distinctUntilChanged(),
        filter(() => this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(val => this.emitChangedFn(val));
  };

  validate(control: AbstractControl): ValidationErrors {
    return this.form.errors;
  }

  writeValue(obj: BreakInputModel): void {
    this.form.patchValue(obj, { emitEvent: false });
  }

  setSelected($event: boolean) {
    this.form.patchValue({ selected: $event });
  }
}
