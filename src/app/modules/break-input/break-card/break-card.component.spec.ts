import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakCardComponent } from './break-card.component';

describe('BreakCardComponent', () => {
  let component: BreakCardComponent;
  let fixture: ComponentFixture<BreakCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreakCardComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
