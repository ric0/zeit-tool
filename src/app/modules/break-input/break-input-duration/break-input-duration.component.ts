import { Component, forwardRef } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validators } from '@angular/forms';
import { AbstractControlValueAccessor } from '../../../shared/components/AbstractControlValueAccessor';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { TimeConstants } from '../../../shared/constants/time-constants';
import { Duration } from 'luxon';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputDurationComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputDurationComponent),
  multi: true
};

@Component({
  selector: 'app-break-input-duration',
  templateUrl: './break-input-duration.component.html',
  styleUrls: ['./break-input-duration.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class BreakInputDurationComponent extends AbstractControlValueAccessor {
  control: FormControl;

  onInitFn = () => {
    this.control = new FormControl(null, [Validators.min(0)]);
    this.control.valueChanges
      .pipe(
        distinctUntilChanged(),
        filter(() => this.control.valid),
        map(minutes => Duration.fromObject({ minutes })),
        takeUntil(this.destroy$)
      )
      .subscribe(val => this.emitChangedFn(val));
  };

  validate(control: AbstractControl): ValidationErrors | null {
    return this.control.errors;
  }

  writeValue(obj: Duration): void {
    if (!obj) {
      obj = TimeConstants.ZERO_SECONDS;
    }
    this.control.patchValue(obj.minutes);
  }
}
