import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakInputDurationComponent } from './break-input-duration.component';

describe('BreakInputDurationComponent', () => {
  let component: BreakInputDurationComponent;
  let fixture: ComponentFixture<BreakInputDurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreakInputDurationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakInputDurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
