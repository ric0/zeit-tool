import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakInputTimeRangeComponent } from './break-input-time-range.component';

describe('BreakInputTimeRangeComponent', () => {
  let component: BreakInputTimeRangeComponent;
  let fixture: ComponentFixture<BreakInputTimeRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreakInputTimeRangeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakInputTimeRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
