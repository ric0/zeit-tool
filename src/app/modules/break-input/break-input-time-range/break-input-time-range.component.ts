import { Component, forwardRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { AbstractControlValueAccessor } from 'src/app/shared/components/AbstractControlValueAccessor';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Duration } from 'luxon';
import { BreakRange } from '../../../shared/models/break-time-range.model';
import { TimeConstants } from '../../../shared/constants/time-constants';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputTimeRangeComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputTimeRangeComponent),
  multi: true
};

@Component({
  selector: 'app-break-input-time-range',
  templateUrl: './break-input-time-range.component.html',
  styleUrls: ['./break-input-time-range.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class BreakInputTimeRangeComponent extends AbstractControlValueAccessor {
  form: FormGroup;
  breakDuration: Duration;
  breakDurationStatus = { negative: false, isLessThan15min: false, moreThan15min: false };

  constructor(private fb: FormBuilder) {
    super();
  }

  private static validateBeginBeforeEnd(control: AbstractControl): ValidationErrors | null {
    const { begin, end } = control.value;

    if (begin && end && (end.diff(begin) as Duration).valueOf() < 0) {
      return { error: 'Begin should be before End' };
    } else {
      return null;
    }
  }

  onInitFn = () => {
    this.form = this.fb.group({
      begin: null,
      end: null
    }, {
      validators: [BreakInputTimeRangeComponent.validateBeginBeforeEnd]
    });

    this.form.valueChanges
      .pipe(
        distinctUntilChanged(),
        tap(val => this.updateBreakDuration(val)),
        filter(() => this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(val => {
        this.emitChangedFn(val);
      });
  };

  writeValue(obj: BreakRange): void {
    if (!obj) {
      obj = {
        begin: TimeConstants.DEFAULT_ARRIVAL_TIME,
        end: TimeConstants.DEFAULT_LEAVING_TIME
      };
    }
    this.form.patchValue(obj, { emitEvent: false });
    this.updateBreakDuration(obj);
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.form.errors;
  }

  private updateBreakDuration({ begin, end }) {
    this.breakDuration = end.diff(begin);
    this.updateBreakDurationStatus(this.breakDuration);
  }

  private updateBreakDurationStatus(breakDuration: Duration) {
    const negative = breakDuration.valueOf() < 0;
    const isLessThan15min = this.breakDuration.valueOf() >= 0
      && this.breakDuration < TimeConstants.FIFTEEN_MINUTES;
    const moreThan15min = !negative && !isLessThan15min;

    this.breakDurationStatus = { negative, isLessThan15min, moreThan15min };
  }
}
