import { Component, forwardRef, Input } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { AbstractControlValueAccessor } from '../../../shared/components/AbstractControlValueAccessor';
import { map, takeUntil } from 'rxjs/operators';
import { BreakInputData, BreakInputType } from '../../../shared/models/break-input.model';
import { Duration } from 'luxon';
import { BreakRange } from '../../../shared/models/break-time-range.model';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => BreakInputComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => BreakInputComponent),
  multi: true
};

@Component({
  selector: 'app-break-input',
  templateUrl: './break-input.component.html',
  styleUrls: ['./break-input.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class BreakInputComponent extends AbstractControlValueAccessor {
  control: FormControl;
  @Input() type: BreakInputType;

  TYPES = BreakInputType;

  onInitFn = () => {
    this.control = new FormControl();

    this.control.valueChanges
      .pipe(
        map((data: BreakRange | Duration): BreakInputData => ({ type: this.type, data })),
        takeUntil(this.destroy$)
      )
      .subscribe(val => this.emitChangedFn(val));
  };

  validate(control: AbstractControl): ValidationErrors | null {
    return this.control.errors;
  }

  writeValue(obj: BreakInputData): void {
    this.control.patchValue(obj.data, { emitEvent: false });
  }
}
