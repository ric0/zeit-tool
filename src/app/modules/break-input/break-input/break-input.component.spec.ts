import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakInputComponent } from './break-input.component';

describe('BreakInputComponent', () => {
  let component: BreakInputComponent;
  let fixture: ComponentFixture<BreakInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreakInputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
