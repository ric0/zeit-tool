import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreakInputTimeRangeComponent } from './break-input-time-range/break-input-time-range.component';
import { BreakCardComponent } from './break-card/break-card.component';
import { BreakInputWrapperComponent } from './break-input-wrapper/break-input-wrapper.component';
import { BreakInputDurationComponent } from './break-input-duration/break-input-duration.component';
import { BreakInputComponent } from './break-input/break-input.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    BreakInputTimeRangeComponent,
    BreakCardComponent,
    BreakInputWrapperComponent,
    BreakInputDurationComponent,
    BreakInputComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ], exports: [
    BreakCardComponent
  ]
})
export class BreakInputModule {
}
