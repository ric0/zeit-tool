import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakInputWrapperComponent } from './break-input-wrapper.component';

describe('BreakInputWrapperComponent', () => {
  let component: BreakInputWrapperComponent;
  let fixture: ComponentFixture<BreakInputWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreakInputWrapperComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakInputWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
