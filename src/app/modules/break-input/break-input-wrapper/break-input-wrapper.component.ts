import { Component, EventEmitter, forwardRef, Output } from '@angular/core';
import { DateTime } from 'luxon';
import { TimeConstants } from '../../../shared/constants/time-constants';
import { AbstractControl, FormArray, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { AbstractControlValueAccessor } from 'src/app/shared/components/AbstractControlValueAccessor';
import { skipWhile, takeUntil } from 'rxjs/operators';
import { BreakInputData, BreakInputType } from '../../../shared/models/break-input.model';
import { MatDialog } from '@angular/material/dialog';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputWrapperComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => BreakInputWrapperComponent),
  multi: true
};

@Component({
  selector: 'app-break-input-wrapper',
  templateUrl: './break-input-wrapper.component.html',
  styleUrls: ['./break-input-wrapper.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class BreakInputWrapperComponent extends AbstractControlValueAccessor {
  @Output() addBreak = new EventEmitter();
  form: FormGroup;
  private isWriteValueActive = false;

  constructor(private fb: FormBuilder, public dialog: MatDialog) {
    super();
  }

  get breaks(): FormArray {
    return this.form.get('breaks') as FormArray;
  }

  onInitFn = () => {
    this.form = this.fb.group({
      breaks: this.fb.array([])
    });

    this.breaks.valueChanges
      .pipe(
        skipWhile(() => this.isWriteValueActive),
        takeUntil(this.destroy$)
      )
      .subscribe(val => this.emitChangedFn(val));
  };

  addDuration() {
    this.addBreakInput({
      type: BreakInputType.DURATION,
      data: TimeConstants.ZERO_SECONDS
    });
  }

  addRange() {
    const now = DateTime.local().set({ second: 0, millisecond: 0 });

    this.addBreakInput({
      type: BreakInputType.RANGE,
      data: {
        begin: now,
        end: now
      }
    });
  }

  removeBreak(i: number) {
    this.breaks.removeAt(i);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.breaks.errors;
  }

  writeValue(obj: BreakInputData[]): void {
    this.isWriteValueActive = true;
    obj.forEach(e => this.addBreakInput(e));
    this.isWriteValueActive = false;
  }

  private addBreakInput(val: BreakInputData) {
    this.breaks.push(this.fb.control(val));
    if (!this.isWriteValueActive) {
      this.addBreak.emit(true);
    }
  }
}
