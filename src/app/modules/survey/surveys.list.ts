export interface SurveyEntry {
  id: number;
  link: string;
  active: boolean;
}

/**
 * make sure every element has a unique id !
 */
export const LIST_OF_SURVEYS: SurveyEntry[] = [
  { id: 0, link: 'https://forms.gle/qhLgxJtMKkYMYhZf9', active: false }
];
