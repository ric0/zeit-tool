import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import {
  SurveyState,
  SurveyStateEntry,
  SurveyStatusType
} from 'src/app/shared/models/survey-state.model';
import { LIST_OF_SURVEYS, SurveyEntry } from './surveys.list';

@Injectable()
export class SurveyService {
  private state: SurveyState;
  current: SurveyEntry;
  showSurvey$: BehaviorSubject<boolean>;

  constructor(private localStorage: LocalStorageService) {
    this.current = LIST_OF_SURVEYS[LIST_OF_SURVEYS.length - 1];
    this.state = this.localStorage.loadSurveyState();
    this.showSurvey$ = new BehaviorSubject(this.shouldShowSurvey());
  }

  accept() {
    this.update('accepted');
  }

  decline() {
    this.update('declined');
  }

  later() {
    this.update('later');
  }

  private update(status: SurveyStatusType) {
    this.updateLocalStorage(status);
    this.showSurvey$.next(false);
  }

  private updateLocalStorage(status: SurveyStatusType) {
    const currentIndex = this.state.entries.findIndex(
      e => e.id === this.current.id
    );
    if (currentIndex === -1) {
      // no entry for currnet survey exists
      this.state.entries.push({ id: this.current.id, status });
    } else {
      // update already saved entry (usually only applies when 'later' is selected)
      this.state.entries[currentIndex].status = status;
    }
    this.localStorage.saveSurveyState(this.state);
  }

  private shouldShowSurvey(): boolean {
    const curr: SurveyStateEntry = this.state.entries.find(
      e => e.id === this.current.id
    );
    return this.current.active && (!curr || (curr && curr.status === 'later'));
  }
}
