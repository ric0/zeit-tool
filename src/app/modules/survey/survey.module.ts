import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { SurveyService } from './survey.service';
import { SurveyComponent } from './survey/survey.component';

@NgModule({
  declarations: [SurveyComponent],
  providers: [SurveyService],
  imports: [CommonModule, SharedModule],
  exports: [SurveyComponent]
})
export class SurveyModule {}
