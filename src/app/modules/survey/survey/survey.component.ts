import { Component, OnInit } from '@angular/core';
import { SurveyService } from '../survey.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {
  surveyLink = '';
  constructor(public survey: SurveyService) {}

  ngOnInit() {
    this.surveyLink = this.survey.current.link;
  }

  onAccept() {
    this.survey.accept();
  }
  onDecline() {
    this.survey.decline();
  }
  onLater() {
    this.survey.later();
  }
}
