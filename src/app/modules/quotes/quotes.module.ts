import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuoteCardComponent } from './components/quote-card/quote-card.component';
import { QuoteOptionsDialogComponent } from './components/quote-options-dialog/quote-options-dialog.component';
import { QuoteOptionsMenuComponent } from './components/quote-options-menu/quote-options-menu.component';
import { QuoteComponent } from './components/quote/quote.component';

@NgModule({
  declarations: [
    QuoteComponent,
    QuoteCardComponent,
    QuoteOptionsDialogComponent,
    QuoteOptionsMenuComponent
  ],
  imports: [CommonModule, RouterModule, SharedModule],
  exports: [QuoteCardComponent]
})
export class QuotesModule {}
