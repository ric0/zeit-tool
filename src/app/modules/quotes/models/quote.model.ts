import { Category } from './category.enum';

export interface Quote {
  id: number;
  cat: Category | string;
  lang: Language | string;
  note: string;
  data: string[];
  author: string;
  dislike: boolean;
}

export enum Language {
  DE = 'de',
  EN = 'en'
}
