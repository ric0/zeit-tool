export enum Category {
  POSITIVE = 'positive',
  INSPIRATIONAL = 'inspirational',
  PROGRAMMING = 'programming',
  MISC = 'misc',
  DARK = 'dark',
  STRANGE = 'strange'
}

export interface CategoryInfo {
  value: string;
  color: string;
  name: string;
}

interface CategoryInfoObject {
  [key: string]: CategoryInfo;
}

const categoryColors = {
  positive: 'green',
  inspirational: 'orange',
  programming: 'blue',
  misc: 'violet',
  dark: 'red',
  strange: 'gray'
};

export function getCategoryColor(cat: Category): string {
  return categoryColors[cat];
}

export function getEveryCategoryInfo(): string[] {
  return Object.keys(categoryColors);
}
