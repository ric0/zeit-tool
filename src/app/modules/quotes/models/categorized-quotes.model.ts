import { Quote } from './quote.model';

export interface CategorizedQuotes {
  [category: string]: Quote[];
}
