import { Component, OnInit } from '@angular/core';
import { Category, getCategoryColor } from '../../models/category.enum';
import { QuotesService } from '../../services/quotes.service';

@Component({
  selector: 'app-quote-card',
  templateUrl: './quote-card.component.html',
  styleUrls: ['./quote-card.component.scss']
})
export class QuoteCardComponent implements OnInit {
  constructor(public quotes: QuotesService) {
  }

  get catColor(): { [key: string]: boolean } {
    if (!this.quotes.current) {
      return { ['']: true };
    }

    const cssColorClass = getCategoryColor(this.quotes.current.cat as Category);
    return { [cssColorClass]: true };
  }

  ngOnInit() {
  }
}
