import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { QuotesService } from '../../services/quotes.service';
import { QuoteOptionsDialogComponent } from '../quote-options-dialog/quote-options-dialog.component';
import { Quote } from '../../models/quote.model';

@Component({
  selector: 'app-quote-options-menu',
  templateUrl: './quote-options-menu.component.html',
  styleUrls: ['./quote-options-menu.component.scss']
})
export class QuoteOptionsMenuComponent implements OnInit {
  @Input() quote: Quote;
  constructor(private dialog: MatDialog, private quotes: QuotesService) {}

  ngOnInit() {}

  onShowNext() {
    this.quotes.next();
  }

  onDislike() {
    this.quotes.dislike();
  }

  openOptionsDialog() {
    this.dialog.open(QuoteOptionsDialogComponent, {
      width: '350px',
      data: this.quote
    });
  }
}
