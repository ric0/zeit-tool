import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteOptionsMenuComponent } from './quote-options-menu.component';

describe('QuoteOptionsMenuComponent', () => {
  let component: QuoteOptionsMenuComponent;
  let fixture: ComponentFixture<QuoteOptionsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuoteOptionsMenuComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteOptionsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
