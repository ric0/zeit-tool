import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Quote } from '../../models/quote.model';
import { QuotesService } from '../../services/quotes.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-quote-options-dialog',
  templateUrl: './quote-options-dialog.component.html',
  styleUrls: ['./quote-options-dialog.component.scss']
})
export class QuoteOptionsDialogComponent {
  categoryMapI18N: any;

  constructor(@Inject(MAT_DIALOG_DATA) public quote: Quote,
              public quotes: QuotesService,
              public translate: TranslateService
  ) {
  }

  get category(): string {
    return this.quote.cat;
  }

  onDislike() {
    this.quotes.dislike();
  }

  onShowNext() {
    this.quotes.next();
  }
}
