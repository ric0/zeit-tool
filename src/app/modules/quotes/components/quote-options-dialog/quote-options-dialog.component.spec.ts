import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteOptionsDialogComponent } from './quote-options-dialog.component';

describe('QuoteOptionsDialogComponent', () => {
  let component: QuoteOptionsDialogComponent;
  let fixture: ComponentFixture<QuoteOptionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuoteOptionsDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteOptionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
