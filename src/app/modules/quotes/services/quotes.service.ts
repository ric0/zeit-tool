import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { differenceWith, shuffle } from 'lodash';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, map, skip, tap } from 'rxjs/operators';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { SettingsService } from 'src/app/services/settings.service';
import { DEFAULT_QUOTE_STATE } from 'src/app/shared/constants/default-quote-state';
import { Settings } from 'src/app/shared/models/settings.model';
import { CategorizedQuotes } from '../models/categorized-quotes.model';
import { Category } from '../models/category.enum';
import { Quote } from '../models/quote.model';
import { QuotesState } from '../models/quotes-state.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {
  private static readonly QUOTES_PATH = environment.assets.data.quotes;
  current: Quote;
  current$: BehaviorSubject<Quote>;
  private allQuotes: CategorizedQuotes = null;
  private remaining: Quote[] = [];
  private state: QuotesState;

  constructor(
    private localStorage: LocalStorageService,
    private settings: SettingsService,
    private http: HttpClient,
    @Inject(LOCALE_ID) public locale: string
  ) {
    this.initQuotes();
    this.listenToSettingsChanges();
    this.listenToSettingsReset();
  }

  next(opts: { saveViewed: boolean } = { saveViewed: true }) {
    this.checkRemaining();
    this.current = this.remaining.pop();
    this.addToViewed(opts);

    this.current$
      ? this.current$.next(this.current)
      : (this.current$ = new BehaviorSubject<Quote>(this.current));
  }

  dislike() {
    this.state.disliked.push(this.current.id);
    this.next();
  }

  resetState() {
    this.localStorage.removeQuotesState();
    this.state = DEFAULT_QUOTE_STATE;
    this.remaining = [];
    this.next();
  }

  private initQuotes() {
    this.state = this.localStorage.loadQuotesState();

    if (this.settings.getValues().quotes.showQuotes) {
      this.loadAllQuotes().pipe(tap(() => {
        this.update();
        this.next();
      })).subscribe();
    }
  }

  private loadAllQuotes(): Observable<any> {
    return this.allQuotes
      ? of(null)
      : this.getAllQuotes().pipe(
        tap(quotes => this.allQuotes = quotes));

  }

  private getAllQuotes(): Observable<CategorizedQuotes> {
    return this.http.get<CategorizedQuotes>(QuotesService.QUOTES_PATH);
  }

  private listenToSettingsChanges() {
    this.settings.values$
      .pipe(
        skip(1),
        filter((s: Settings) => s.quotes.showQuotes),
        map((s: Settings) => s.quotes),
        distinctUntilChanged()
      )
      .subscribe(() => {
        this.loadAllQuotes().subscribe(() => {
          this.update();
          this.next({ saveViewed: false });
        });
      });
  }

  private listenToSettingsReset() {
    this.settings.reset$
      .pipe(filter(val => (val ? val.clearCache : false)))
      .subscribe(() => {
        this.resetState();
      });
  }

  private checkRemaining() {
    if (this.remaining.length === 0) {
      this.state.viewed = [];
      this.update();
    }
  }

  private addToViewed({ saveViewed }) {
    if (this.current && saveViewed) {
      this.state.viewed.push(this.current.id);
      this.localStorage.saveQuotesState(this.state);
    }
  }

  /**
   * update remaining quotes-array, which will be displayed.
   *
   * - only select quotes with a specified category
   * - sorts out disliked and viewed ones
   * - shuffles the remaining quotes
   */
  private update() {
    const categories = this.getSelectedCategories();

    this.remaining = this.getQuotesWithSpecific(categories);
    this.remaining = differenceWith(this.remaining, this.state.disliked, this.cmp);
    this.remaining = differenceWith(this.remaining, this.state.viewed, this.cmp);

    this.remaining = shuffle(this.remaining);
  }

  private getSelectedCategories(): Category[] {
    const cats = this.settings.getValues().quotes.selectedCategories;
    return cats && cats.length > 0 ? cats : this.getEveryCat();
  }

  private getEveryCat(): Category[] {
    return Object.keys(this.allQuotes) as Category[];
  }

  private getQuotesWithSpecific(cats: Category[]): Quote[] {
    const quotesWithSpecificCategory = [];
    cats
      .filter(cat => this.allQuotes[cat]) // check if category exists
      .forEach(cat => {
        quotesWithSpecificCategory.push(...this.allQuotes[cat]);
      });

    return quotesWithSpecificCategory;
  }

  private cmp(q: Quote, id: number) {
    return q.id === id;
  }
}
