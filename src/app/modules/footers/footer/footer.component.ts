import { Component, OnInit } from '@angular/core';
import { VERSION } from '../../../../environments/version';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  version = VERSION.version;
  branch = VERSION.branch;
  constructor() { }

  ngOnInit() {
  }

}
