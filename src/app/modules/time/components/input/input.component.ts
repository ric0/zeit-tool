import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateTime, Duration } from 'luxon';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { SettingsService } from 'src/app/services/settings.service';
import { TimeService } from 'src/app/services/time.service';
import { TimeConstants } from 'src/app/shared/constants/time-constants';
import { TimeSelection } from 'src/app/shared/enums/time-selection.enum';
import { InputModel } from 'src/app/shared/models/input.model';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, OnDestroy {
  form: FormGroup;
  TimeSelection = TimeSelection;
  SIX_HOURS = TimeConstants.SIX_HOURS;
  NINE_HOURS = TimeConstants.NINE_HOURS;
  TEN_HOURS = TimeConstants.TEN_HOURS;

  private destroy$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    public settings: SettingsService,
    private time: TimeService
  ) {
  }

  get isShowQuotes(): boolean {
    return this.settings.getValues().quotes.showQuotes;
  }

  ngOnInit() {
    this.setupForm();
    this.setFormValues();
    this.listenToFormChanges();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getCurrentTime(): DateTime {
    return DateTime.local();
  }

  setInputTo(key: string, val: DateTime | Duration) {
    this.form.patchValue({ [key]: val });
  }

  setTimeSelectionTo(val: TimeSelection) {
    this.form.patchValue({ timeSelection: val });
  }

  private setupForm() {
    this.form = this.fb.group({
      arrivalTime: [null, [Validators.required]],
      timeSelection: [null, [Validators.required]],
      workingTime: [null],
      leaveTime: [null],
      break: [null]
    });
  }

  private setFormValues() {
    const initialValues: InputModel = this.localStorage.loadInput();
    this.form.patchValue(initialValues, { emitEvent: false });
  }

  private listenToFormChanges() {
    this.form.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(() => this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(input => this.time.updateValues(input));
  }
}
