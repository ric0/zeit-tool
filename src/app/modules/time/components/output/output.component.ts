import { Component, OnDestroy, OnInit } from '@angular/core';
import { DateTime } from 'luxon';
import { Subject, Subscription, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { calcTimeDifference } from 'src/app/services/calc-helper/time-diff-helper';
import { TimeService } from 'src/app/services/time.service';
import { TimeConstants } from 'src/app/shared/constants/time-constants';
import { Balance } from 'src/app/shared/enums/balance.enum';
import { OutputModel } from 'src/app/shared/models/output.model';
import { TimeDiff } from 'src/app/shared/models/time-diff';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss']
})
export class OutputComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private tickerSub: Subscription;

  output: OutputModel;
  remainingTime: TimeDiff;

  Balance = Balance;

  constructor(public time: TimeService) {}

  ngOnInit() {
    const sub = this.time.output$
      .pipe(takeUntil(this.destroy$))
      .subscribe(output => (this.output = output));

    this.createTickingTime();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (!!this.tickerSub) {
      this.tickerSub.unsubscribe();
    }
  }

  private createTickingTime() {
    if (!!this.tickerSub) {
      this.tickerSub.unsubscribe();
    }
    this.remainingTime = this.getRemainingTime();

    this.tickerSub = timer(0, 1000).subscribe(() => {
      this.remainingTime = this.getRemainingTime();
    });
  }

  private getRemainingTime(): TimeDiff {
    if (this.output && this.output.leaveTime) {
      return calcTimeDifference(DateTime.local(), this.output.leaveTime);
    } else {
      return {
        balance: Balance.Neutral,
        value: TimeConstants.ZERO_SECONDS
      };
    }
  }
}
