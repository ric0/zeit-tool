import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { FeierabendModule } from '../feierabend/feierabend.module';
import { QuotesModule } from '../quotes/quotes.module';
import { VisualDisplayModule } from '../visual-display/visual-display.module';
import { DescriptionComponent } from './components/description/description.component';
import { InputComponent } from './components/input/input.component';
import { OutputComponent } from './components/output/output.component';
import { TimeComponent } from './components/time/time.component';
import { BreakInputModule } from '../break-input/break-input.module';

@NgModule({
  declarations: [
    InputComponent,
    OutputComponent,
    TimeComponent,
    DescriptionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BreakInputModule,
    QuotesModule,
    VisualDisplayModule,
    FeierabendModule
  ],
  exports: [TimeComponent]
})
export class TimeModule {
}
