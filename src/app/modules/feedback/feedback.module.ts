import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackComponent } from './feedback/feedback.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FeedbackFormComponent } from './feedback-form/feedback-form.component';

export const routes: Routes = [{ path: '', component: FeedbackComponent }];

@NgModule({
  declarations: [FeedbackComponent, FeedbackFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class FeedbackModule {
}
