import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { VERSION } from '../../../../environments/version';

const enum FeedbackType {
  BUG = 'BUG', // de: bug
  FEATURE = 'FEATURE', // de: Feature
  IMPROVEMENT = 'IMPROVEMENT', // de: Verbesserung
  CRITICISM = 'CRITICISM' // de: Kritik
}

interface FeedbackInputModel {
  type: FeedbackType;
  title: string;
  message: string;
  isDebugInfo: boolean;
}

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.scss']
})
export class FeedbackFormComponent implements OnInit, OnDestroy {
  feedbackTypeValues = ['BUG', 'FEATURE', 'IMPROVEMENT', 'CRITICISM'];
  mailtoLink$ = new Subject<string>();
  form: FormGroup;
  private destroy$ = new Subject<void>();

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      type: [null, Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required],
      isDebugInfo: [true, Validators.required]
    });

    this.form.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      filter(() => this.form.valid),
      takeUntil(this.destroy$)
    ).subscribe((input: FeedbackInputModel) => {
      const mailtoLink = this.composeMailtoLink(input);
      this.mailtoLink$.next(mailtoLink);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  private composeMailtoLink(input: FeedbackInputModel) {
    const subject = `[${input.type}] ${input.title}`;
    const body = `
${input.message}
${input.isDebugInfo ? this.getDebugInfo() : ''}
Gitlab-Issue-Info:
/label ~NEW
    `;

    return `mailto:${environment.links.feedback}`
      + `?subject=${encodeURIComponent(subject.trim())}&body=${encodeURIComponent(body.trim())}`;
  }

  private getDebugInfo() {
    return `

DEBUG-INFO - Version:
${JSON.stringify(VERSION)}

DEBUG-INFO - LocalStorage:
${JSON.stringify(localStorage)}
    `;
  }
}
