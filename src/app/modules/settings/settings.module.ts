import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContentWarningDialogComponent } from './content-warning-dialog/content-warning-dialog.component';
import { SettingsFormComponent } from './settings-form/settings-form.component';
import { SettingsQuoteCategoriesComponent } from './settings-quote-categories/settings-quote-categories.component';
import { SettingsQuotesComponent } from './settings-quotes/settings-quotes.component';
import { routes } from './settings-routing';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    SettingsComponent,
    SettingsFormComponent,
    SettingsQuotesComponent,
    SettingsQuoteCategoriesComponent,
    ContentWarningDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class SettingsModule {
}
