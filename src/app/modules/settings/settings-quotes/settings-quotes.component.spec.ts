import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsQuotesComponent } from './settings-quotes.component';

describe('QuotesComponent', () => {
  let component: SettingsQuotesComponent;
  let fixture: ComponentFixture<SettingsQuotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsQuotesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
