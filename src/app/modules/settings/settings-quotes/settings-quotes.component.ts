import { Component, forwardRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { AbstractControlValueAccessor } from 'src/app/shared/components/AbstractControlValueAccessor';
import { QuotesService } from '../../quotes/services/quotes.service';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => SettingsQuotesComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => SettingsQuotesComponent),
  multi: true
};

@Component({
  selector: 'app-settings-quotes',
  templateUrl: './settings-quotes.component.html',
  styleUrls: ['./settings-quotes.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class SettingsQuotesComponent extends AbstractControlValueAccessor {
  constructor(private fb: FormBuilder, private quotes: QuotesService) {
    super();
  }
  form: FormGroup;

  onInitFn = () => {
    this.form = this.fb.group({
      showQuotes: null,
      selectedCategories: {}
    });
    this.listenToValueChanges();
  };

  private listenToValueChanges() {
    this.form.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(300),
        distinctUntilChanged(),
        filter(() => this.form.valid)
      )
      .subscribe(value => {
        this.emitChangedFn(value);
      });
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.form.errors;
  }

  writeValue(obj: any): void {
    this.form.patchValue(obj ? obj : {}, { emitEvent: false });
  }

  onFocusOut() {
    this.emitTouchedFn();
  }

  resetQuotesState() {
    this.quotes.resetState();
  }
}
