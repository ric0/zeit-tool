import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentWarningDialogComponent } from './content-warning-dialog.component';

describe('ContentWarningDialogComponent', () => {
  let component: ContentWarningDialogComponent;
  let fixture: ComponentFixture<ContentWarningDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContentWarningDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentWarningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
