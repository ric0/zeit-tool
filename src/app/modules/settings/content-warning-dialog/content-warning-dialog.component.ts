import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-content-warning-dialog',
  templateUrl: './content-warning-dialog.component.html',
  styleUrls: ['./content-warning-dialog.component.scss']
})
export class ContentWarningDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ContentWarningDialogComponent>) {}

  ngOnInit() {}
}
