import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  constructor(@Inject(LOCALE_ID) public locale: string,
              private settings: SettingsService) {
  }

  ngOnInit() {
  }

  resetSettings() {
    this.settings.reset();
  }
}
