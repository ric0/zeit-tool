import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  takeUntil
} from 'rxjs/operators';
import { SettingsService } from 'src/app/services/settings.service';
import { TimeService } from 'src/app/services/time.service';
import { DEFAULT_SETTINGS } from 'src/app/shared/constants/default-settings';
import { Settings } from 'src/app/shared/models/settings.model';

@Component({
  selector: 'app-settings-form',
  templateUrl: './settings-form.component.html',
  styleUrls: ['./settings-form.component.scss']
})
export class SettingsFormComponent implements OnInit, OnDestroy {
  form: FormGroup;

  private destroy$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private settings: SettingsService,
    private time: TimeService
  ) {}

  ngOnInit() {
    this.setupForm();
    this.setFormValues();
    this.listenToValueChanges();

    this.settings.reset$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.resetSettings();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private setupForm() {
    this.form = this.fb.group({
      defaultWorkingTime: ['', [Validators.required]],
      enableAutomaticArrivalTime: [false, [Validators.required]],
      enableTabCloseWarning: [false, [Validators.required]],
      quotes: [null, Validators.required]
    });
  }

  private setFormValues() {
    const initialValues = this.settings.getValues();
    this.form.patchValue(initialValues, { emitEvent: false });
  }

  private listenToValueChanges() {
    this.form.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(() => this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe((newSettings: Settings) => {
        this.settings.update(newSettings);
        this.snackbar.open('Einstellungen gespeichert', null, {
          duration: 2000
        });
      });
  }

  resetInput() {
    this.time.resetValues();
    this.snackbar.open('Eingabe zurückgesetzt', null, { duration: 2000 });
  }

  private resetSettings() {
    this.form.patchValue(DEFAULT_SETTINGS, { emitEvent: false });
    this.snackbar.open('Einstellungen zurückgesetzt', null, { duration: 2000 });
  }

  deleteLocalStorage() {
    this.settings.clearLocalStorage();
    this.snackbar.open('Local-Storage gelöscht', null, { duration: 2000 });
  }
}
