import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsQuoteCategoriesComponent } from './settings-quote-categories.component';

describe('QuoteCategoriesComponent', () => {
  let component: SettingsQuoteCategoriesComponent;
  let fixture: ComponentFixture<SettingsQuoteCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsQuoteCategoriesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsQuoteCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
