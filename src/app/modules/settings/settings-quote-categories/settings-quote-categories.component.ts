import { Component, forwardRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { filter, map, takeUntil } from 'rxjs/operators';
import { AbstractControlValueAccessor } from 'src/app/shared/components/AbstractControlValueAccessor';
import { convertArrayToObject } from 'src/app/shared/util';
import { Category, getEveryCategoryInfo } from '../../quotes/models/category.enum';
import { ContentWarningDialogComponent } from '../content-warning-dialog/content-warning-dialog.component';
import { TranslateService } from '@ngx-translate/core';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => SettingsQuoteCategoriesComponent),
  multi: true
};

const CUSTOM_VALIDATOR = {
  provide: NG_VALIDATORS,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => SettingsQuoteCategoriesComponent),
  multi: true
};

@Component({
  selector: 'app-settings-quote-categories',
  templateUrl: './settings-quote-categories.component.html',
  styleUrls: ['./settings-quote-categories.component.scss'],
  providers: [CUSTOM_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class SettingsQuoteCategoriesComponent extends AbstractControlValueAccessor {
  categories = getEveryCategoryInfo();
  categoryMapI18N: any;
  form: FormGroup;

  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              public translate: TranslateService) {
    super();
  }

  onInitFn = () => {
    this.initForm();
    this.listenToValueChanges();
  };

  /**
   * Create an formGroup-Object
   * e.g:
   *      {
   *        positive: null,
   *        ...
   *        dark: null
   *      }
   */
  initForm() {
    const obj: { [formControlName: string]: any } = this.categories
      .reduce((acc, curr) => {
        acc[curr] = null;
        return acc;
      }, {});

    this.form = this.fb.group(obj);
  }

  writeValue(arr: Category[]): void {
    const obj = convertArrayToObject(arr, true);
    this.form.reset({}, { emitEvent: false });
    this.form.patchValue(obj, { emitEvent: false });
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.form.errors;
  }

  onFocusOut() {
    this.emitTouchedFn();
  }

  showContentWarning(event: MatCheckboxChange, category: string) {
    // the checkboxes are generated generically, which means
    // every checkbox gets this function passed, show the
    // content-warning ONLY on the DARK category
    if (event.checked && category === Category.DARK) {
      const dialogRef = this.dialog.open(ContentWarningDialogComponent, {
        disableClose: true,
        width: '350px'
      });

      dialogRef.afterClosed().subscribe((result: { dark: boolean }) => {
        this.form.patchValue(result);
      });
    }
  }

  private listenToValueChanges() {
    this.form.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.form.valid),
        map(selectedCategories => {
          return Object.keys(selectedCategories).filter(
            catName => selectedCategories[catName]
          );
        })
      )
      .subscribe((selectedCategories: Category[]) => {
        this.emitChangedFn(selectedCategories);
      });
  }
}
