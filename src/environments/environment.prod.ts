import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  assets: {
    data: {
      quotes: 'assets/data/quotes.json'
    }
  },
  links: {
    feedback: 'incoming+ric0-zeit-tool-12988819-eA5snRsMZuwyorZSTtsL-issue@incoming.gitlab.com'
  }
};
