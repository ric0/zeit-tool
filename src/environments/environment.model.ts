export interface Environment {
  production: boolean;
  assets: {
    data: {
      quotes: string;
    };
  };
  links: {
    feedback: string;
  };
}
