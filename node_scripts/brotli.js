const fs = require('fs');
const brotli = require('brotli');

const outFolder = './dist';
fs.readdir(outFolder, { withFileTypes: true }, (err, files) => {
  if (err) {
    console.log(err);
    return;
  }
  files = files // TODO remove assignment when netlify upgrades NODEJS > 11
    .filter(
      dirent =>
        dirent && !dirent.name.endsWith('.gz') && !dirent.name.endsWith('.br')
    )
    .map(dirent => resolveDirectory(outFolder, dirent));
  //.flat(Infinity)
  flattenDeep(files) // TODO remove when netlify upgrades NODEJS > 11
    .forEach(file => {
      console.log(file);
      const compressedBuffer = brotli.compress(fs.readFileSync(file));
      fs.writeFileSync(`${file}.br`, compressedBuffer);
    });
});

function resolveDirectory(folderPath, dirent) {
  if (dirent.isFile()) {
    return `${folderPath}/${dirent.name}`;
  }

  const dirEntries = fs.readdirSync(`${folderPath}/${dirent.name}`, {
    withFileTypes: true
  });
  return dirEntries
    .filter(
      ent => ent && !ent.name.endsWith('.gz') && !ent.name.endsWith('.br')
    )
    .map(ent => {
      return resolveDirectory(`${folderPath}/${dirent.name}`, ent);
    });
}

/**
 * Polyfill for Array.prototype.flat()
 *
 * TODO remove when netlify upgrades NODEJS > 11
 *
 * @param {*} arr to flatten
 */
function flattenDeep(arr1) {
  return arr1.reduce(
    (acc, val) =>
      Array.isArray(val) ? acc.concat(flattenDeep(val)) : acc.concat(val),
    []
  );
}
