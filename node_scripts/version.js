/**
 * https://medium.com/@amcdnl/version-stamping-your-app-with-the-angular-cli-d563284bb94d
 */
console.info('Generating version information...');

const { writeFileSync } = require('fs');
const { resolve, relative } = require('path');


const gitInfo = require('git-describe').gitDescribeSync();

const semVer = require('../package.json').version;
gitInfo.version = semVer;

if (process.env.NETLIFY) {
  gitInfo.branch = process.env.BRANCH;
} else {
  gitInfo.branch = require('git-branch').sync();
}

const file = resolve(__dirname, '..', 'src', 'environments', 'version.ts');
writeFileSync(file,
  `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = ${JSON.stringify(gitInfo, null, 2)};
/* tslint:enable */
`, { encoding: 'utf-8' });

console.log(`Wrote version info ${gitInfo.raw} to ${relative(resolve(__dirname, '..'), file)}`);
