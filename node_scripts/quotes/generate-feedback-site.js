/**
 * This NodeJS-script uses the generated json-object (created from the csv)
 * to inject the json-object into the feedback site
 */
console.info('Injecting JSON into feedback.html ...');

const fs = require('fs');
const path = require('path');

const jsonFilePath = path.resolve('generated/quotes.json');
const htmlFilePath = path.resolve('template/feedback.html');
const generatedOutputFilePath = path.resolve('generated/feedback.html');

const quotes = fs.readFileSync(jsonFilePath, { encoding: 'UTF-8' });
const html = fs.readFileSync(htmlFilePath, { encoding: 'UTF-8' });

const generatedHTML = html.replace(/QUOTES/g, JSON.stringify(quotes));
fs.writeFile(generatedOutputFilePath, generatedHTML, 'UTF-8', () => {
});
console.log('Wrote', htmlFilePath);
