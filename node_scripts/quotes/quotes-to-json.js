/**
 * This NodeJS-script converts the csv-data
 * into an json-object and a typescript-file
 *
 * Usage:
 *   IMPORTANT: Run the following command in the projects root directory
 *
 *   $ npm run postinstall
 *
 */
console.info('Converting CSV to JSON ...');

const fs = require('fs');
const path = require('path');
const csv = require('csvtojson');

const outputFolderNameForFeedback = 'node_scripts/quotes/generated';
const jsonFileName = 'quotes.json';
const feedbackFilePath = path.resolve(outputFolderNameForFeedback, jsonFileName);
const csvFilePath = path.resolve('src/assets/data/quotes.csv');
const assetsFilePath = path.resolve('src/assets/data', jsonFileName);

csv({
  delimiter: ';',
  noheader: false
}).fromFile(csvFilePath)
  .then(rawJson => {

    const quotes = rawJson
      .map(q => {
        const quote = {
          ...q,
          id: +q.id,
          cat: q.cat === '' ? 'misc' : q.cat,
          data: [q.quote1, q.quote2, q.quote3, q.quote4]
        };
        quote.data = quote.data.filter(line => (line !== null && line !== ''));
        delete quote.quote1;
        delete quote.quote2;
        delete quote.quote3;
        delete quote.quote4;

        quote.dislike = false;

        return quote;
      });

    try {
      fs.mkdirSync(outputFolderNameForFeedback);
      console.info('Created directory', outputFolderNameForFeedback);
    } catch (e) {
      console.info(`Directory (${outputFolderNameForFeedback}) already exists.`);
    }

    writeFile(feedbackFilePath, quotes);
    writeFile(assetsFilePath, categorizeQuotes(quotes));
  });

function categorizeQuotes(quotes) {
  // TODO make it generic, to avoid having to change cats manually
  const categoriezedQuotes = {
    positive: [],
    misc: [],
    programming: [],
    dark: [],
    inspirational: [],
    strange: []
  };

  quotes.forEach(q => {
    try {
      categoriezedQuotes[q.cat].push(q);
    } catch (e) {
      console.error('Malformed Quote, please check: ', { ...q }, e);
    }
  });

  return categoriezedQuotes;
}


function writeFile(absoluteFsPath, jsonData) {
  fs.writeFileSync(absoluteFsPath, JSON.stringify(jsonData), 'utf8');
  console.log('Wrote to', absoluteFsPath);
}
