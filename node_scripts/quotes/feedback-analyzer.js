const fs = require('fs');

fs.readdir('./feedback-data', (err, files) => {
  if (!err) files.forEach(analyze);
  else console.error(err);
});

const quotes = JSON.parse(
  fs.readFileSync('./generated/quotes.json', { encoding: 'UTF-8' })
);

function analyze(filename) {
  const feedback = JSON.parse(
    fs.readFileSync(`./feedback-data/${filename}`, {
      encoding: 'UTF-8'
    })
  );

  const res = feedback
    .filter(e => e.feedback.value !== 'good' || e.feedback.note != '')
    .map(f => {
      return f.feedback.note === '' ? { ...f, feedback: f.feedback.value } : f;
    })
    .map(f => {
      const quote = quotes.find(q => +q.id === +f.quoteid);
      if (quote) {
        return {
          id: +quote.id,
          f: f.feedback,
          q: quote.data
        };
      } else {
        return {
          id: +f.quoteid,
          f: f.feedback,
          q: 'NOT FOUND or REMOVED'
        };
      }
    });

  res.forEach(e => console.log(e));
  console.log(
    'Number of results which are (not good || with comment):',
    res.length
  );
}
